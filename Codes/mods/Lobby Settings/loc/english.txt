{
	"ls_menu_reject_vac" : "Reject VAC banned players",
	"ls_menu_reject_vac_help" : "This restriction doesn't concern your direct Steam friends.",

	"ls_menu_reject_play_time_threshold" : "Reject by play time",
	"ls_menu_reject_play_time_threshold_help" : "Players under the specified play time threshold (in hours) won't be allowed.",

	"ls_menu_reject_unknown_play_time" : "Reject unknown play time",
	"ls_menu_reject_unknown_play_time_help" : "Spare those with private profiles/game details, or not.\nThis restriction doesn't concern your direct Steam friends.",

	"ls_menu_reject_bogus_outfits" : "Reject bogus outfits",
	"ls_menu_reject_bogus_outfits_help" : "Kick players sending outfit values that are out of bounds.",

	"ls_menu_max_players" : "Max players",
	"ls_menu_max_players_help" : "Set this to 3 if you want to keep a slot for a bot.",

	"ls_menu_infamy_permission" : "Infamy limit",
	"ls_menu_infamy_permission_help" : "",

	"ls_menu_ban_ready_forcers" : "Ban ready forcers",
	"ls_menu_ban_ready_forcers_help" : "Clients trying to force host's ready state will be banned.",

	"ls_peer_has_a_bogus_outfit" : "$NAME has a bogus outfit.",
	"ls_peer_tried_to_force_ready" : "$NAME tried to force ready state."
}
