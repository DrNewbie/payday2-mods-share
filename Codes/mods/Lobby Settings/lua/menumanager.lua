local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

_G.LobbySettings = _G.LobbySettings or {}
LobbySettings._path = ModPath
LobbySettings._data_path = SavePath .. 'lobby_settings.txt'
LobbySettings.friends = {}
LobbySettings.load_level = Global.load_level
LobbySettings.settings = {
	always_allow_friends = true,
	reject_vac = false,
	reject_play_time_threshold = 0,
	reject_unknown_play_time = false,
	reject_bogus_outfits = true,
	ban_ready_forcers = false,
}

function LobbySettings:save()
	local file = io.open(self._data_path, 'w+')
	if file then
		local settings = self.settings
		settings.lobby_permission = Global.game_settings.permission
		settings.reputation_permission = Global.game_settings.reputation_permission
		settings.infamy_permission = Global.game_settings.infamy_permission
		settings.max_players = Global.game_settings.max_players
		settings.max_bots = Global.game_settings.max_bots
		settings.job_plan = Global.game_settings.job_plan
		settings.kick_option = Global.game_settings.kick_option
		settings.drop_in_option = Global.game_settings.drop_in_option

		file:write(json.encode(settings))
		file:close()
	end
end

function LobbySettings:load()
	local file = io.open(self._data_path, 'r')
	if file then
		for k, v in pairs(json.decode(file:read('*all')) or {}) do
			self.settings[k] = v
		end
		file:close()
	end
	Global.game_settings.permission = self.settings.lobby_permission or 'friends_only'
	Global.game_settings.infamy_permission = self.settings.infamy_permission or 0
	Global.game_settings.reputation_permission = self.settings.reputation_permission or 0
	Global.game_settings.max_players = self.settings.max_players or tweak_data.max_players
	Global.game_settings.max_bots = self.settings.max_bots or CriminalsManager.MAX_NR_TEAM_AI
	Global.game_settings.job_plan = self.settings.job_plan or -1
	Global.game_settings.kick_option = self.settings.kick_option or 1
	Global.game_settings.drop_in_option = self.settings.drop_in_option or 1
	Global.game_settings.drop_in_allowed = Global.game_settings.drop_in_option ~= 0
end

function LobbySettings:reset_status(include_banned)
	local ls_status = Global.ls_status
	if type(ls_status) ~= 'table' then
		return
	end

	for steam_id, status in pairs(ls_status) do
		if status == true or type(status) == 'number' and (include_banned or status ~= 9) then
			ls_status[steam_id] = nil
		end
	end
end

function LobbySettings:reset()
	self.settings.infamy_permission = nil
	self.settings.reputation_permission = nil
	self.settings.reject_play_time_threshold = 0

	self:save()
	self:load()
end

function LobbySettings:init_friends_list()
	self.friends = {}
	if Steam and Steam:logged_on() then
		for _, friend in ipairs(Steam:friends() or {}) do
			self.friends[friend:id()] = true
		end
	end
end

function LobbySettings:restore_max_nr_team_ai()
	CriminalsManager.MAX_NR_TEAM_AI = self.original_MAX_NR_TEAM_AI
end

function LobbySettings:patch_max_nr_team_ai()
	if self.load_level then
		CriminalsManager.MAX_NR_TEAM_AI = Global.game_settings.max_bots
	end
end

Hooks:Add('LocalizationManagerPostInit', 'LocalizationManagerPostInit_LobbySettings', function(loc)
	local language_filename

	local modname_to_language = {
		['PAYDAY 2 THAI LANGUAGE Mod'] = 'thai.txt',
	}
	for _, mod in pairs(BLT and BLT.Mods:Mods() or {}) do
		language_filename = mod:IsEnabled() and modname_to_language[mod:GetName()]
		if language_filename then
			break
		end
	end

	if not language_filename then
		for _, filename in pairs(file.GetFiles(LobbySettings._path .. 'loc/')) do
			local str = filename:match('^(.*).txt$')
			if str and Idstring(str) and Idstring(str):key() == SystemInfo:language():key() then
				language_filename = filename
				break
			end
		end
	end

	if language_filename then
		loc:load_localization_file(LobbySettings._path .. 'loc/' .. language_filename)
	end
	loc:load_localization_file(LobbySettings._path .. 'loc/english.txt', false)
end)

local ls_original_menucallbackhandler_dialogclearprogressyes = MenuCallbackHandler._dialog_clear_progress_yes
function MenuCallbackHandler:_dialog_clear_progress_yes()
	LobbySettings:reset()
	ls_original_menucallbackhandler_dialogclearprogressyes(self)
end

local ls_original_menucallbackhandler_increaseinfamous = MenuCallbackHandler._increase_infamous
function MenuCallbackHandler:_increase_infamous(...)
	ls_original_menucallbackhandler_increaseinfamous(self, ...)
	LobbySettings.settings.reputation_permission = Global.game_settings.reputation_permission
	LobbySettings:save()
end

Hooks:Add('MenuManagerInitialize', 'MenuManagerInitialize_LobbySettings', function(menu_manager)

	LobbySettings:init_friends_list()
	LobbySettings:load()

	local funcs = {
		'choice_crimenet_drop_in',
		'choice_crimenet_lobby_job_plan',
		'choice_crimenet_lobby_permission',
		'choice_crimenet_lobby_reputation_permission',
		'choice_drop_in',
		'choice_lobby_job_plan',
		'choice_lobby_permission',
		'choice_lobby_reputation_permission',
		'choice_kicking_option',
	}
	for _, func in pairs(funcs) do
		local original_function = MenuCallbackHandler[func]
		MenuCallbackHandler[func] = function(...)
			original_function(...)
			LobbySettings:save()
		end
	end

	function MenuCallbackHandler:ls_choice_lobby_infamy_permission(item)
		local value = item:value()
		Global.game_settings.infamy_permission = value
		if LobbySettings.egs_item_infamy and item ~= LobbySettings.egs_item_infamy then
			LobbySettings.egs_item_infamy:set_value(value)
		end
		LobbySettings:save()
	end

	function MenuCallbackHandler:ls_choice_lobby_max_players(item)
		local max_players = item:value()
		Global.game_settings.max_players = max_players
		if LobbySettings.egs_item_max_players and item ~= LobbySettings.egs_item_max_players then
			LobbySettings.egs_item_max_players:set_value(max_players)
		end
		if Network:is_server() then
			managers.network:session():chk_server_joinable_state()
		end
		LobbySettings:save()
	end

	function MenuCallbackHandler:ls_choice_lobby_max_bots(item)
		local max_bots = item:value()
		Global.game_settings.team_ai = max_bots > 0
		Global.game_settings.team_ai_option = max_bots
		Global.game_settings.max_bots = max_bots
		if LobbySettings.egs_item_max_bots and item ~= LobbySettings.egs_item_max_bots then
			LobbySettings.egs_item_max_bots:set_value(max_bots)
		end
		LobbySettings:save()
	end

	function MenuCallbackHandler:ls_choice_lobby_reject_vac(item)
		local value = item:value() == 'on'
		LobbySettings.settings.reject_vac = value
		if LobbySettings.egs_item_reject_vac and item ~= LobbySettings.egs_item_reject_vac then
			LobbySettings.egs_item_reject_vac:set_value(value and 'on' or 'off')
		end
		LobbySettings:reset_status(true)
		LobbySettings:save()
	end

	function MenuCallbackHandler:ls_choice_lobby_reject_play_time_threshold(item)
		local value = item:value()
		LobbySettings.settings.reject_play_time_threshold = value
		if LobbySettings.egs_item_reject_play_time_threshold and item ~= LobbySettings.egs_item_reject_play_time_threshold then
			LobbySettings.egs_item_reject_play_time_threshold:set_value(value)
		end

		if LobbySettings.egs_item_reject_unknown_play_time then
			LobbySettings.egs_item_reject_unknown_play_time:set_enabled(value ~= 0)
			if value == 0 then
				LobbySettings.egs_item_reject_unknown_play_time:set_value('off')
			end
			managers.menu:active_menu().logic:refresh_node('main')
		end

		LobbySettings:reset_status()
		LobbySettings:save()
	end

	function MenuCallbackHandler:ls_choice_lobby_reject_unknown_play_time(item)
		local value = item:value() == 'on'
		LobbySettings.settings.reject_unknown_play_time = value
		LobbySettings:reset_status()
		LobbySettings:save()
	end

	function MenuCallbackHandler:ls_choice_lobby_reject_bogus_outfits(item)
		local value = item:value() == 'on'
		LobbySettings.settings.reject_bogus_outfits = value
		if LobbySettings.egs_item_reject_bogus_outfits and item ~= LobbySettings.egs_item_reject_bogus_outfits then
			LobbySettings.egs_item_reject_bogus_outfits:set_value(value and 'on' or 'off')
		end
		LobbySettings:save()
	end

	function MenuCallbackHandler:ls_choice_lobby_ban_ready_forcers(item)
		local value = item:value() == 'on'
		LobbySettings.settings.ban_ready_forcers = value
		if LobbySettings.egs_item_ban_ready_forcers and item ~= LobbySettings.egs_item_ban_ready_forcers then
			LobbySettings.egs_item_ban_ready_forcers:set_value(value and 'on' or 'off')
		end
		LobbySettings:save()
	end

	function MenuCallbackHandler:ls_infamy_check(data)
		return data:value() <= managers.experience:current_rank()
	end
end)

local function _insertmenuitems(nodes)
	if Global.statistics_manager and Global.statistics_manager.play_time.minutes > 0 then
		-- qued
	else
		DelayedCalls:Add('DelayedModLS_insertmenuitems', 1, function()
			_insertmenuitems(nodes)
		end)
		return
	end

	LobbySettings.egs_item_infamy = LobbySettings:insert_infamy_limiter(nodes.edit_game_settings)
	LobbySettings.egs_item_max_players = LobbySettings:insert_player_limiter(nodes.edit_game_settings)
	LobbySettings.egs_item_max_bots = LobbySettings:insert_bot_limiter(nodes.edit_game_settings)
	LobbySettings.egs_item_reject_vac = LobbySettings:insert_reject_VAC(nodes.edit_game_settings)
	LobbySettings.egs_item_reject_play_time_threshold = LobbySettings:insert_reject_play_time(nodes.edit_game_settings)
	LobbySettings.egs_item_reject_unknown_play_time = LobbySettings:insert_reject_unknown_play_time(nodes.edit_game_settings)
	LobbySettings.egs_item_reject_bogus_outfits = LobbySettings:insert_reject_bogus_outfits(nodes.edit_game_settings)
	LobbySettings.egs_item_ban_ready_forcers = LobbySettings:insert_ban_ready_forcers(nodes.edit_game_settings)
end

Hooks:Add('MenuManagerBuildCustomMenus', 'MenuManagerBuildCustomMenus_LobbySettings', function(menu_manager, nodes)
	if nodes.edit_game_settings then
		_insertmenuitems(nodes)
	end
end)

function LobbySettings:insert_infamy_limiter(node)
	local data = {
		type = 'MenuItemMultiChoice'
	}

	for i = 0, tweak_data.infamy.ranks do
		local option = {
			_meta = 'option',
			localize = false,
			text_id = i,
			value = i,
			visible_callback = 'ls_infamy_check'
		}
		table.insert(data, option)
	end

	local params = {
		name = 'ls_multi_infamy_permission',
		text_id = 'ls_menu_infamy_permission',
		help_id = 'ls_menu_infamy_permission_help',
		callback = 'ls_choice_lobby_infamy_permission',
		localize = true,
		visible_callback = 'is_multiplayer'
	}

	local item = node:create_item(data, params)
	item:set_value(Global.game_settings.infamy_permission)
	item:set_callback_handler(node.callback_handler)

	for k, v in pairs(node._items) do
		if v._parameters.name == 'lobby_reputation_permission' then
			table.insert(node._items, k, item)
			break
		end
	end

	return item
end

function LobbySettings:insert_player_limiter(node)
	local data = {
		type = 'MenuItemMultiChoice'
	}

	for i = 1, CriminalsManager.MAX_NR_CRIMINALS do
		local option = {
			_meta = 'option',
			localize = false,
			text_id = i,
			value = i
		}
		table.insert(data, option)
	end

	local params = {
		name = 'ls_multi_max_players',
		text_id = 'ls_menu_max_players',
		help_id = 'ls_menu_max_players_help',
		callback = 'ls_choice_lobby_max_players',
		localize = true,
		visible_callback = 'is_multiplayer'
	}

	local item = node:create_item(data, params)
	item:set_value(Global.game_settings.max_players)
	item:set_callback_handler(node.callback_handler)

	for k, v in pairs(node._items) do
		if v._parameters.name == 'lobby_permission' then
			table.insert(node._items, k, item)
			break
		end
	end

	return item
end

function LobbySettings:insert_bot_limiter(node)
	local data = {
		type = 'MenuItemMultiChoice'
	}

	for i = 0, LobbySettings.original_MAX_NR_TEAM_AI do
		local option = {
			_meta = 'option',
			localize = false,
			text_id = i,
			value = i
		}
		table.insert(data, option)
	end

	local params = {
		name = 'ls_multi_max_bots',
		text_id = 'menu_toggle_ai',
		callback = 'ls_choice_lobby_max_bots',
		localize = true
	}

	local item = node:create_item(data, params)
	item:set_value(Global.game_settings.max_bots)
	item:set_callback_handler(node.callback_handler)

	for k, v in pairs(node._items) do
		if v._parameters.name == 'toggle_ai' then
			node._items[k] = item
			break
		end
	end

	return item
end

local checkbox_data = {
	type = 'CoreMenuItemToggle.ItemToggle',
	{
		_meta = 'option',
		icon = 'guis/textures/menu_tickbox',
		value = 'on',
		x = 24,
		y = 0,
		w = 24,
		h = 24,
		s_icon = 'guis/textures/menu_tickbox',
		s_x = 24,
		s_y = 24,
		s_w = 24,
		s_h = 24
	},
	{
		_meta = 'option',
		icon = 'guis/textures/menu_tickbox',
		value = 'off',
		x = 0,
		y = 0,
		w = 24,
		h = 24,
		s_icon = 'guis/textures/menu_tickbox',
		s_x = 0,
		s_y = 24,
		s_w = 24,
		s_h = 24
	}
}

function LobbySettings:insert_reject_VAC(node)
	local params = {
		name = 'ls_toggle_reject_vac',
		text_id = 'ls_menu_reject_vac',
		help_id = 'ls_menu_reject_vac_help',
		callback = 'ls_choice_lobby_reject_vac',
		localize = true,
		visible_callback = 'is_multiplayer'
	}

	local item = node:create_item(checkbox_data, params)
	item:set_value(self.settings.reject_vac and 'on' or 'off')
	item:set_callback_handler(node.callback_handler)

	table.insert(node._items, item)

	return item
end

function LobbySettings:insert_reject_unknown_play_time(node)
	local params = {
		name = 'ls_toggle_reject_unknown_play_time',
		text_id = 'ls_menu_reject_unknown_play_time',
		help_id = 'ls_menu_reject_unknown_play_time_help',
		callback = 'ls_choice_lobby_reject_unknown_play_time',
		localize = true,
		visible_callback = 'is_multiplayer'
	}

	local item = node:create_item(checkbox_data, params)
	item:set_enabled(self.settings.reject_play_time_threshold ~= 0)
	item:set_value(self.settings.reject_play_time_threshold ~= 0 and self.settings.reject_unknown_play_time and 'on' or 'off')
	item:set_callback_handler(node.callback_handler)

	table.insert(node._items, item)

	return item
end

function LobbySettings:insert_reject_bogus_outfits(node)
	local params = {
		name = 'ls_toggle_reject_bogus_outfits',
		text_id = 'ls_menu_reject_bogus_outfits',
		help_id = 'ls_menu_reject_bogus_outfits_help',
		callback = 'ls_choice_lobby_reject_bogus_outfits',
		localize = true,
		visible_callback = 'is_multiplayer'
	}

	local item = node:create_item(checkbox_data, params)
	item:set_value(self.settings.reject_bogus_outfits and 'on' or 'off')
	item:set_callback_handler(node.callback_handler)

	table.insert(node._items, item)

	return item
end

function LobbySettings:insert_ban_ready_forcers(node)
	local params = {
		name = 'ls_toggle_ban_ready_forcers',
		text_id = 'ls_menu_ban_ready_forcers',
		help_id = 'ls_menu_ban_ready_forcers_help',
		callback = 'ls_choice_lobby_ban_ready_forcers',
		localize = true,
		visible_callback = 'is_multiplayer'
	}

	local item = node:create_item(checkbox_data, params)
	item:set_value(self.settings.ban_ready_forcers and 'on' or 'off')
	item:set_callback_handler(node.callback_handler)

	table.insert(node._items, item)

	return item
end

function LobbySettings:insert_reject_play_time(node)
	local data = {
		type = 'MenuItemMultiChoice'
	}

	local mpt = Global.statistics_manager and Global.statistics_manager.play_time.minutes / 60 or 0
	local to = math.min(1000, math.floor(mpt / 50))
	for i = 0, to do
		local option = {
			_meta = 'option',
			localize = false,
			text_id = i * 50,
			value = i * 50,
		}
		table.insert(data, option)
	end

	local params = {
		name = 'ls_multi_reject_play_time_threshold',
		text_id = 'ls_menu_reject_play_time_threshold',
		help_id = 'ls_menu_reject_play_time_threshold_help',
		callback = 'ls_choice_lobby_reject_play_time_threshold',
		localize = true,
		visible_callback = 'is_multiplayer'
	}

	local item = node:create_item(data, params)
	item:set_value(self.settings.reject_play_time_threshold)
	item:set_callback_handler(node.callback_handler)

	table.insert(node._items, item)

	return item
end

local function _current_menu_has_buy_button()
	local menu = managers.menu:active_menu()
	local selnode = menu and menu.logic and menu.logic:selected_node()
	local buy_contract_item = selnode and selnode:item('buy_contract')
	return not not buy_contract_item
end

local function _set_difficulty(node)
	local difficulty = node:item('difficulty')
	if difficulty then
		if not difficulty:parameters().gui_node then
			if not _current_menu_has_buy_button() then
				DelayedCalls:Add('DelayedModLS_changecontractdifficulty', 0, function()
					_set_difficulty(node)
				end)
			end
			return
		end

		difficulty:set_value(LobbySettings.settings.difficulty)
		MenuCallbackHandler:change_contract_difficulty(difficulty)

		local toggle_one_down = node:item('toggle_one_down')
		if toggle_one_down then
			toggle_one_down:set_value(LobbySettings.settings.one_down and 'on' or 'off')
			MenuCallbackHandler:choice_crimenet_one_down(toggle_one_down)
		end
	end
end

local function _patch_node(node, data)
	data = data or {}

	if Global.game_settings.single_player then
		_set_difficulty(node)
		LobbySettings:insert_bot_limiter(node)
	elseif data.smart_matchmaking then
		-- qued
	elseif not data.server then
		_set_difficulty(node)
		if node:item('lobby_reputation_permission'):visible() then
			LobbySettings:insert_infamy_limiter(node)
			LobbySettings:insert_player_limiter(node)
		end
		if node:item('toggle_ai'):visible() then
			LobbySettings:insert_bot_limiter(node)
		end
	end
end

local ls_original_menucrimenetcontractinitiator_modifynode = MenuCrimeNetContractInitiator.modify_node
function MenuCrimeNetContractInitiator:modify_node(original_node, data)
	local node = ls_original_menucrimenetcontractinitiator_modifynode(self, original_node, data)
	_patch_node(node, data)
	return node
end

local ls_original_menuskirmishcontractinitiator_modifynode = MenuSkirmishContractInitiator.modify_node
function MenuSkirmishContractInitiator:modify_node(original_node, data)
	local node = ls_original_menuskirmishcontractinitiator_modifynode(self, original_node, data)
	_patch_node(node, data)
	return node
end
