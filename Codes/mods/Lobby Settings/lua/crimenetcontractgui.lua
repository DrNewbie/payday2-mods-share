local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

local ls_original_crimenetcontractgui_init = CrimeNetContractGui.init
function CrimeNetContractGui:init(...)
	ls_original_crimenetcontractgui_init(self, ...)

	if alive(self._briefing_len_panel) then
		self._briefing_len_panel:set_top(self._briefing_len_panel:top() - 8)
	end
end

local ls_original_crimenetcontractgui_setdifficultyid = CrimeNetContractGui.set_difficulty_id
function CrimeNetContractGui:set_difficulty_id(difficulty_id)
	LobbySettings.settings.difficulty = difficulty_id
	LobbySettings:save()
	ls_original_crimenetcontractgui_setdifficultyid(self, difficulty_id)
end

local ls_original_crimenetcontractgui_setonedown = CrimeNetContractGui.set_one_down
function CrimeNetContractGui:set_one_down(one_down)
	LobbySettings.settings.one_down = one_down
	LobbySettings:save()
	ls_original_crimenetcontractgui_setonedown(self, one_down)
end
