local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

function LobbySettings:is_outfit_ok(outfit)
	local skills = type(outfit) == 'table' and outfit.skills
	if type(skills) ~= 'table' then
		return
	elseif type(skills.skills) ~= 'table' then
		return
	elseif type(skills.specializations) ~= 'table' then
		return
	elseif #skills.skills ~= #tweak_data.skilltree.trees then
		return
	end

	local perk_id = tonumber(skills.specializations[1] or -1)
	if perk_id < 1 or perk_id > #tweak_data.skilltree.specializations then
		return
	end

	local perk_level = tonumber(skills.specializations[2] or -1)
	if perk_level < 0 or perk_level > 9 then
		return
	end

	local total = 0
	for _, v in pairs(skills.skills) do
		total = total + tonumber(v or 0)
	end
	if total > 120 then
		return
	end

	return true
end

local ls_original_connectionnetworkhandler_syncoutfit = ConnectionNetworkHandler.sync_outfit
function ConnectionNetworkHandler:sync_outfit(outfit_string, outfit_version, outfit_signature, sender)
	if Network:is_server() and LobbySettings.settings.reject_bogus_outfits then
		local peer = self._verify_sender(sender)
		if not peer then
			return
		end

		local outfit = managers.blackmarket:unpack_outfit_from_string(outfit_string)
		if not LobbySettings:is_outfit_ok(outfit) then
			log('[Lobby Settings] Rejected player (' .. peer:user_id() .. ') outfit:' .. tostring(outfit_string))
			if managers.chat then
				local msg = managers.localization:text('ls_peer_has_a_bogus_outfit', { NAME = peer:name() })
				managers.chat:_receive_message(ChatManager.GAME, 'Lobby Settings', msg, tweak_data.system_chat_color)
			end
			managers.network:session():send_to_peers('kick_peer', peer:id(), 0)
			managers.network:session():on_peer_kicked(peer, peer:id(), 0)
			return
		end
	end

	ls_original_connectionnetworkhandler_syncoutfit(self, outfit_string, outfit_version, outfit_signature, sender)
end

local ls_original_connectionnetworkhandler_setmemberready = ConnectionNetworkHandler.set_member_ready
function ConnectionNetworkHandler:set_member_ready(peer_id, ready, mode, outfit_versions_str, sender)
	if mode == 1 and peer_id == 1 and Network:is_server() then
		local peer = self._verify_sender(sender)
		if peer then
			if managers.chat then
				local msg = managers.localization:text('ls_peer_tried_to_force_ready', { NAME = peer:name() })
				managers.chat:_receive_message(ChatManager.GAME, 'Lobby Settings', msg, tweak_data.system_chat_color)
			end

			if LobbySettings.settings.ban_ready_forcers then
				if not managers.ban_list:banned(peer:user_id()) then
					managers.ban_list:ban(peer:user_id(), peer:name())
				end
				managers.network:session():send_to_peers('kick_peer', peer:id(), 6)
				managers.network:session():on_peer_kicked(peer, peer:id(), 6)
			end
		end

		return
	end

	ls_original_connectionnetworkhandler_setmemberready(self, peer_id, ready, mode, outfit_versions_str, sender)
end
