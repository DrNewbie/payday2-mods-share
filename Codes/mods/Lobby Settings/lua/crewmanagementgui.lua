local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

local ls_original_crewmanagementgui_init = CrewManagementGui.init
function CrewManagementGui:init(...)
	LobbySettings:restore_max_nr_team_ai()
	ls_original_crewmanagementgui_init(self, ...)
	LobbySettings:patch_max_nr_team_ai()
end

local ls_original_crewmanagementgui_creatememberloadoutmap = CrewManagementGui._create_member_loadout_map
function CrewManagementGui:_create_member_loadout_map(...)
	LobbySettings:restore_max_nr_team_ai()
	local result = ls_original_crewmanagementgui_creatememberloadoutmap(self, ...)
	LobbySettings:patch_max_nr_team_ai()
	return result
end
