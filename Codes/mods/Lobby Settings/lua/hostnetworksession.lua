local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

local ls_status = Global.ls_status or {}
Global.ls_status = ls_status

local ls_original_hostnetworksession_onjoinrequestreceived = HostNetworkSession.on_join_request_received
function HostNetworkSession:on_join_request_received(...)
	local host_steam_id = self._state_data.local_peer:user_id() or ''
	local sender = select(select('#', ...), ...)
	if not sender then
		log('[Lobby Settings] Join request ignored: missing sender')
		return
	end

	local steam_id = sender:ip_at_index(0)
	if Global.game_settings.single_player then
		log('[Lobby Settings] Join request denied for ' .. tostring(steam_id) .. ': single player mode')
		self._state:_send_request_denied(sender, 5, host_steam_id)
		return
	end

	if self:amount_of_players() >= Global.game_settings.max_players then
		log('[Lobby Settings] Join request denied for ' .. tostring(steam_id) .. ': lobby is full')
		self._state:_send_request_denied(sender, 5, host_steam_id)
		return
	end

	local is_friend = LobbySettings.friends[steam_id]
	local settings = LobbySettings.settings
	if is_friend and settings.always_allow_friends then
		ls_original_hostnetworksession_onjoinrequestreceived(self, ...)
		return
	end

	local peer_rank = select(6, ...)
	if peer_rank < (Global.game_settings.infamy_permission or 0) then
		log('[Lobby Settings] Join request denied for ' .. tostring(steam_id) .. ': infamy level too low')
		self._state:_send_request_denied(sender, 6, host_steam_id)
		return
	end

	local status = ls_status[steam_id]
	if status == true then
		ls_original_hostnetworksession_onjoinrequestreceived(self, ...)
		return

	elseif type(status) == 'number' then
		log('[Lobby Settings] Join request denied for ' .. tostring(steam_id) .. ': cached reject')
		self._state:_send_request_denied(sender, status, host_steam_id)
		return
	end

	if settings.reject_play_time_threshold > 0 then
		-- qued
	elseif settings.reject_vac then
		-- qued
	else
		ls_original_hostnetworksession_onjoinrequestreceived(self, ...)
		return
	end

	local t = TimerManager:wall_running():time()
	if status and t - tonumber(status) < 30 then
		log('[Lobby Settings] Join request ignored for ' .. tostring(steam_id) .. ': verification already in progress, waiting for http reply')
		return
	end
	ls_status[steam_id] = tostring(t)

	local params = {...}
	dohttpreq('http://steamcommunity.com/profiles/' .. steam_id .. '/?xml=1',

		function (page)
			if type(page) ~= 'string' then
				log('[Lobby Settings] Join request ignored for ' .. tostring(steam_id) .. ': no Steam reply')
				ls_status[steam_id] = nil
				return
			end

			if settings.reject_vac then
				local vac_nr = page:match('<vacBanned>(%d+)</vacBanned>')
				local is_ok = not vac_nr or vac_nr == '0'

				if not is_ok then
					ls_status[steam_id] = 9
					log('[Lobby Settings] Join request denied for ' .. tostring(steam_id) .. ': VAC banned')
					self._state:_send_request_denied(sender, 9, host_steam_id)
					return
				end
			end

			if settings.reject_play_time_threshold > 0 then
				local is_ok, retcode, reason
				local hours_nr = page:match('<mostPlayedGame>.-<gameLink>.-218620.-</gameLink>.-<hoursOnRecord>([%d,.]+)</hoursOnRecord>')
				hours_nr = type(hours_nr) == 'string' and hours_nr:gsub(',' , '')
				if hours_nr then
					retcode = 6
					reason = 'play time too low'
					is_ok = tonumber(hours_nr) >= settings.reject_play_time_threshold
				elseif is_friend then
					is_ok = true
				else
					retcode = 3
					reason = 'hidden play time'
					is_ok = not settings.reject_unknown_play_time
				end

				if not is_ok then
					ls_status[steam_id] = retcode
					log('[Lobby Settings] Join request denied for ' .. tostring(steam_id) .. ': ' .. tostring(reason))
					self._state:_send_request_denied(sender, retcode, host_steam_id)
					return
				end
			end

			ls_status[steam_id] = true
			log('[Lobby Settings] Join request approved for ' .. tostring(steam_id))
			ls_original_hostnetworksession_onjoinrequestreceived(self, unpack(params))
		end

	)
end
