local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

LobbySettings.original_MAX_NR_TEAM_AI = CriminalsManager.MAX_NR_TEAM_AI
if Global.game_settings then
	Global.game_settings.max_bots = Global.game_settings.max_bots or CriminalsManager.MAX_NR_TEAM_AI
	LobbySettings:patch_max_nr_team_ai()
end
