local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

local capp_original_hostnetworksession_chkdropinpeer = HostNetworkSession.chk_drop_in_peer
function HostNetworkSession:chk_drop_in_peer(dropin_peer)
	if dropin_peer:expecting_dropin() and not CrewAbilityPiedPiper:peer_has_capp(dropin_peer) then
		CrewAbilityPiedPiper.filter_ability = true
	end

	local result = capp_original_hostnetworksession_chkdropinpeer(self, dropin_peer)

	CrewAbilityPiedPiper.filter_ability = false

	return result
end
