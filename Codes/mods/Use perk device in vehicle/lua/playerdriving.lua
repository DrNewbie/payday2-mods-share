local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

local pdv_original_playerdriving_init = PlayerDriving.init
function PlayerDriving:init(...)
	local projectile_entry = managers.blackmarket:equipped_projectile()
	local projectile_tweak = tweak_data.blackmarket.projectiles[projectile_entry]
	self.pdv_projectile_ability = projectile_tweak.ability

	pdv_original_playerdriving_init(self, ...)
end

local pdv_original_playerdriving_updatecheckactionsdriver = PlayerDriving._update_check_actions_driver
function PlayerDriving:_update_check_actions_driver(t, dt, input)
	pdv_original_playerdriving_updatecheckactionsdriver(self, t, dt, input)

	if input.btn_projectile_press and self.pdv_projectile_ability then
		self:_check_action_use_ability(t, input)
	end
end

local pdv_original_playerdriving_updatecheckactionspassengernoshoot = PlayerDriving._update_check_actions_passenger_no_shoot
function PlayerDriving:_update_check_actions_passenger_no_shoot(t, dt, input)
	pdv_original_playerdriving_updatecheckactionspassengernoshoot(self, t, dt, input)

	if input.btn_projectile_press and self.pdv_projectile_ability then
		self:_check_action_use_ability(t, input)
	end
end
