local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

local _is_buying

Hooks:Add('LocalizationManagerPostInit', 'LocalizationManagerPostInit_SearchInventory', function(loc)
	local text = string.trim(loc:text('menu_cn_filters_sidebar', {btn = ''}))
	loc:add_localized_strings({si_filter = text}, true)
end)

local si_original_blackmarketgui_close = BlackMarketGui.close
function BlackMarketGui:close()
	if self.si_qki then
		self:si_cancel_filter_change()
	end

	si_original_blackmarketgui_close(self)
	SearchInventory:reset_filters()
end

local si_original_blackmarketgui_setup = BlackMarketGui._setup
function BlackMarketGui:_setup(is_start_page, component_data)
	_is_buying = false

	local dont_add_extra_button
	if not component_data then
		dont_add_extra_button = true
	elseif component_data.topic_id == 'bm_menu_buy_mask_title' then
		_is_buying = true
	elseif component_data.custom_callback then
		dont_add_extra_button = true
	elseif component_data.buying_weapon then
		_is_buying = true
	elseif managers.blackmarket:get_hold_crafted_item() then
		dont_add_extra_button = true
	else
		local category = component_data.category
		if category == 'primaries'
		or category == 'secondaries'
		or category == 'masks'
		or component_data.topic_id == 'bm_menu_customize_mask_title'
		or component_data.topic_id == 'bm_menu_melee_weapons'
		then
			-- qued
		else
			dont_add_extra_button = true
		end
	end
	self.si_extra_button_added = not dont_add_extra_button

	si_original_blackmarketgui_setup(self, is_start_page, component_data)

	if dont_add_extra_button then
		return
	end

	if alive(self.mws_bp_switch_panel) and self.mws_bp_switch_panel:parent() == self._panel then
		self.mws_bp_switch_panel:set_height(self.mws_bp_switch_panel:height() + 20)
	else
		self.mws_bp_switch_panel = self._panel:panel({
			w = self._buttons:w(),
			h = 30,
		})
		self.mws_bp_switch_panel:set_right(self._panel:w())
	end

	local ref_panel = component_data.topic_id == 'bm_menu_customize_mask_title' and self._extra_options_panel or self._weapon_info_panel
	self.mws_bp_switch_panel:set_bottom(ref_panel:y() + 2)

	local btn_data = {
		prio = 1,
		name = 'si_filter',
		pc_btn = 'menu_toggle_filters',
		callback = callback(self, self, 'si_filter_callback')
	}
	self.si_filter_btn = BlackMarketGuiButtonItem:new(self.mws_bp_switch_panel, btn_data, 10)
	self.si_filter_btn._data.prio = 5 -- ugly trick for double-click
	self._btns['si_filter'] = self.si_filter_btn

	self.si_info_text_id = #self._info_texts + 1
	self.si_info_text = self._panel:text({
		w = self.mws_bp_switch_panel:w(),
		h = 25,
		text = SearchInventory:get_filter_str(),
		name = 'info_text_' .. self.si_info_text_id,
		layer = 1,
		font_size = tweak_data.menu.pd2_small_font_size,
		font = tweak_data.menu.pd2_small_font,
		color = tweak_data.screen_colors.risk
	})
	self.si_info_text:set_bottom(self.mws_bp_switch_panel:top() + 2)
	self.si_info_text:set_left(self.mws_bp_switch_panel:left())

	self.si_info_texts_bg = self._panel:rect({
		alpha = 0.2,
		visible = false,
		layer = 0,
		color = Color.black
	})
	self.si_info_texts_bg:set_shape(self.si_info_text:shape())

	self:show_btns(self._selected_slot)
end

local si_original_blackmarketgui_showbtns = BlackMarketGui.show_btns
function BlackMarketGui:show_btns(...)
	si_original_blackmarketgui_showbtns(self, ...)

	local btn = self._btns['si_filter']
	if btn then
		btn:set_text_params()
		btn:show()
		self._controllers_pc_mapping[Idstring(btn._data.pc_btn):key()] = btn
	end
end

function BlackMarketGui:si_filter_callback(data)
	self:si_start_rename_item()
end

function BlackMarketGui:si_start_rename_item()
	if not self.si_renaming_item then
		self.si_info_texts_bg:set_visible(true)
		self.si_renaming_item = SearchInventory:get_filter_str()
		self.si_original_search_text = self.si_renaming_item

		self.si_qki = QuickKeyboardInputHusk:new(self._ws, self.si_info_text, {
			changed_callback = callback(self, self, 'si_accept_filter_change'),
			changing_callback = callback(self, self, 'si_filter_is_changing'),
			cancel_callback = callback(self, self, 'si_cancel_filter_change')
		})

		self:update_info_text()
	end
end

function BlackMarketGui:si_filter_is_changing(txt)
	self.si_renaming_item = txt
	self:update_info_text()
	SearchInventory:set_filters(self.si_renaming_item)
	self:si_refresh()
end

function BlackMarketGui:si_accept_filter_change()
	if alive(self._panel) and self.si_renaming_item then
		self.si_info_texts_bg:set_visible(false)
		self.si_renaming_item = nil
		self.si_qki = nil
	end
end

function BlackMarketGui:si_cancel_filter_change()
	if self.si_renaming_item then
		self.si_info_text:set_text(self.si_original_search_text)
		SearchInventory:set_filters(self.si_original_search_text)
		self.si_info_texts_bg:set_visible(false)
		self.si_renaming_item = nil
		self.si_qki = nil

		self._one_frame_input_delay = true

		self:update_info_text()
		self:si_refresh()
	end
end

local si_original_blackmarketgui_mousepressed = BlackMarketGui.mouse_pressed
function BlackMarketGui:mouse_pressed(button, x, y)
	if self._enabled and self._data[1].category ~= 'deployables' then
		if managers.menu_scene and managers.menu_scene:input_focus() then
			-- qued
		elseif self.si_renaming_item then
			self:si_cancel_filter_change()
			return
		end
	end

	return si_original_blackmarketgui_mousepressed(self, button, x, y)
end

local si_original_blackmarketgui_updateinfotext = BlackMarketGui.update_info_text
function BlackMarketGui:update_info_text()
	si_original_blackmarketgui_updateinfotext(self)

	if self.si_qki then
		self.si_info_text:set_text(self.si_renaming_item)
	end
end

local si_original_blackmarketgui_moveup = BlackMarketGui.move_up
function BlackMarketGui:move_up()
	if not self.si_renaming_item then
		return si_original_blackmarketgui_moveup(self)
	end
end

local si_original_blackmarketgui_movedown = BlackMarketGui.move_down
function BlackMarketGui:move_down()
	if not self.si_renaming_item then
		return si_original_blackmarketgui_movedown(self)
	end
end

local si_original_blackmarketgui_moveleft = BlackMarketGui.move_left
function BlackMarketGui:move_left()
	if not self.si_renaming_item then
		return si_original_blackmarketgui_moveleft(self)
	end
end

local si_original_blackmarketgui_moveright = BlackMarketGui.move_right
function BlackMarketGui:move_right()
	if not self.si_renaming_item then
		return si_original_blackmarketgui_moveright(self)
	end
end

local si_original_blackmarketgui_nextpage = BlackMarketGui.next_page
function BlackMarketGui:next_page(...)
	if not self.si_renaming_item then
		return si_original_blackmarketgui_nextpage(self, ...)
	end
end

local si_original_blackmarketgui_previouspage = BlackMarketGui.previous_page
function BlackMarketGui:previous_page(...)
	if not self.si_renaming_item then
		return si_original_blackmarketgui_previouspage(self, ...)
	end
end

local si_original_blackmarketgui_pressbutton = BlackMarketGui.press_button
function BlackMarketGui:press_button(...)
	if not self.si_renaming_item then
		return si_original_blackmarketgui_pressbutton(self, ...)
	end
end

local si_original_blackmarketgui_specialbtnpressed = BlackMarketGui.special_btn_pressed
function BlackMarketGui:special_btn_pressed(button)
	if not self.si_renaming_item then
		if button == Idstring('menu_toggle_filters') then
			self.si_ignore_input_until_key_release = true
		end
		return si_original_blackmarketgui_specialbtnpressed(self, button)
	end
end

local si_original_blackmarketgui_setselectedtab = BlackMarketGui.set_selected_tab
function BlackMarketGui:set_selected_tab(...)
	si_original_blackmarketgui_setselectedtab(self, ...)
	self:si_refresh()
end

local si_original_blackmarketgui_postreload = BlackMarketGui._post_reload
function BlackMarketGui:_post_reload()
	si_original_blackmarketgui_postreload(self)
	self:si_refresh()
end

function BlackMarketGui:si_refresh()
	local tab = self._tabs[self._selected]
	if tab then
		local nr = 0
		local identifier = tab._data and tab._data.identifier
		if identifier == self.identifiers.weapon or identifier == self.identifiers.melee_weapon then
			nr = (self._selected - 1) * tweak_data.gui.WEAPON_ROWS_PER_PAGE * tweak_data.gui.WEAPON_COLUMNS_PER_PAGE
		elseif identifier == self.identifiers.mask then
			nr = (self._selected - 1) * tweak_data.gui.MASK_ROWS_PER_PAGE * tweak_data.gui.MASK_COLUMNS_PER_PAGE
		elseif identifier == self.identifiers.mask_mod then
			-- qued
		end

		for i, slot in pairs(tab._slots) do
			slot.si_index = i + nr
			slot:refresh()
		end
	end
end

local si_original_blackmarketguislotitem_init = BlackMarketGuiSlotItem.init
function BlackMarketGuiSlotItem:init(main_panel, data, x, y, w, h)
	si_original_blackmarketguislotitem_init(self, main_panel, data, x, y, w, h)

	if self.rect_bg then
		self.rect_bg:set_visible(false)
	end
end

function BlackMarketGuiSlotItem:si_match(filters)
	if self.si_text and type(filters) == 'table' then
		for _, filter in ipairs(filters) do
			if not self.si_text:find(filter) then
				return false
			end
		end
	end
	return true
end

local si_original_blackmarketguislotitem_refresh = BlackMarketGuiSlotItem.refresh
function BlackMarketGuiSlotItem:refresh()
	si_original_blackmarketguislotitem_refresh(self)

	if self.si_index and alive(self._panel) then
		self.si_text = self.si_text or managers.blackmarket:si_get_search_string(self, _is_buying)
		local alpha = self:si_match(SearchInventory.filters) and 1 or 0.03
		self._panel:set_alpha(alpha)
		if self._bitmap and self._data.category == 'textures' then
			self._bitmap:set_visible(alpha == 1)
		end
	end
end
