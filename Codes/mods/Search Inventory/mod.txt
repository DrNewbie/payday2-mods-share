{
	"blt_version" : 2,
	"name" : "Search Inventory",
	"description" : "Highlight inventory items matching your keywords.",
	"author" : "TdlQ",
	"image" : "tdlq.dds",
	"color" : "0.52 1 0",
	"contact" : "",
	"version" : "16",
	"priority" : 15,
	"simple_update_url" : "http://pd2mods.z77.fr/update/SearchInventory.zip",
	"simple_dependencies" : {
		"QuickKeyboardInput": "http://pd2mods.z77.fr/update/QuickKeyboardInput.zip"
	},
	"hooks" : [
		{
			"hook_id" : "lib/managers/blackmarketmanager",
			"script_path" : "lua/blackmarketmanager.lua"
		},
		{
			"hook_id" : "lib/managers/menu/blackmarketgui",
			"script_path" : "lua/blackmarketgui.lua"
		}
	]
}
