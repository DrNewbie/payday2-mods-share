local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

local access_type_all = {
	acrobatic = true,
	walk = true
}

local ignored_groups = {
	Phalanx_minion = true,
	Phalanx_vip = true
}

local function clear_table(tbl)
	local k = next(tbl)
	while k do
		tbl[k] = nil
		k = next(tbl)
	end
end

local function overwrite_table(src, dst)
	clear_table(dst)
	for k, v in pairs(src) do
		dst[k] = v
	end
end

local function get_special_group(special_type)
	for group, units in pairs(tweak_data.group_ai.unit_categories) do
		if units.special_type == special_type and not units.is_captain then
			units.access = access_type_all
			return units
		end
	end
end

function MutatorEnemyReplacer:modify_unit_categories(group_ai_tweak, difficulty_index)
	local overridden_enemy = self:get_override_enemy()

	for key, value in pairs(group_ai_tweak.special_unit_spawn_limits) do
		if key == overridden_enemy then
			group_ai_tweak.special_unit_spawn_limits[key] = math.huge
		end
	end

	local unit_group = get_special_group(overridden_enemy)
	if not unit_group then
		unit_group = self['_get_unit_group_' .. overridden_enemy](self, difficulty_index)
	end

	for group, units in pairs(group_ai_tweak.unit_categories) do
		if not ignored_groups[group] then
			group_ai_tweak.unit_categories[group] = unit_group
		end
	end
end

local fm_original_mutatormedidozer_modifyunitcategories = MutatorMediDozer.modify_unit_categories
function MutatorMediDozer:modify_unit_categories(group_ai_tweak, ...)
	local original_medic_M4 = clone(group_ai_tweak.unit_categories.medic_M4)
	local original_medic_R870 = clone(group_ai_tweak.unit_categories.medic_R870)

	fm_original_mutatormedidozer_modifyunitcategories(self, group_ai_tweak, ...)

	overwrite_table(original_medic_M4, group_ai_tweak.unit_categories.medic_M4)
	overwrite_table(original_medic_R870, group_ai_tweak.unit_categories.medic_R870)
end

local fm_original_mutatortitandozers_getunitgrouptitandozer = MutatorTitandozers._get_unit_group_titandozer
function MutatorTitandozers:_get_unit_group_titandozer(...)
	local unit_group = get_special_group('tank')
	local flavours = unit_group and table.map_keys(unit_group.unit_types)

	local result = fm_original_mutatortitandozers_getunitgrouptitandozer(self, ...)

	for _, flavour in pairs(flavours) do
		if not result.unit_types[flavour] then
			local _, v = next(result.unit_types)
			result.unit_types[flavour] = v
		end
	end

	return result
end
