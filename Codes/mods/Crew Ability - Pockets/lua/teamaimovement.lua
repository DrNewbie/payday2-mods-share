local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

function TeamAIMovement:downed()
	local interaction = self._unit:interaction()
	return interaction._active and interaction.tweak_data == 'revive'
end
