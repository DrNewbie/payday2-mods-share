local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

local _from_interact
local _from_interact_start
local _override_has_special_equipment
local _can_be_taken_by_bot
local _given_to_bot

local cap_original_baseinteractionext_interactstart = BaseInteractionExt.interact_start
function BaseInteractionExt:interact_start(...)
	_from_interact_start = true
	local result, timer = cap_original_baseinteractionext_interactstart(self, ...)
	_from_interact_start = false
	return result, timer
end

local cap_original_useinteractionext_interact = UseInteractionExt.interact
function UseInteractionExt.interact(...)
	_from_interact = true
	local result = cap_original_useinteractionext_interact(...)
	_from_interact = false
	return result
end

local cap_original_baseinteractionext_interactinterupt = BaseInteractionExt.interact_interupt
function BaseInteractionExt:interact_interupt(player, complete)
	if not complete then
		_can_be_taken_by_bot = false
		self.cap_given_to_bot = nil
	end
	cap_original_baseinteractionext_interactinterupt(self, player, complete)
end

local cap_original_baseinteractionext_canselect = BaseInteractionExt.can_select
function BaseInteractionExt:can_select(player, locator)
	_override_has_special_equipment = not not self._tweak_data.special_equipment_block
	local result = cap_original_baseinteractionext_canselect(self, player, locator)
	_override_has_special_equipment = false
	return result
end

local cap_original_baseinteractionext_caninteract = BaseInteractionExt.can_interact
function BaseInteractionExt:can_interact(player)
	_can_be_taken_by_bot = false
	_override_has_special_equipment = not not self._tweak_data.special_equipment_block
	local result = cap_original_baseinteractionext_caninteract(self, player)
	_override_has_special_equipment = false

	if _from_interact and _can_be_taken_by_bot then
		local special_equipment = self._special_equipment or self._tweak_data.special_equipment_block
		if not special_equipment then
			-- qued
		elseif Network:is_client() then
			managers.network:session():send_to_host('sync_interacted', self._unit, self._unit:id(), 'stash_in_bot_pocket', 1)
			_given_to_bot = true
		elseif CrewAbilityPockets:give_item_to_teamAI(special_equipment, player:position()) then
			_given_to_bot = true
		else
			result = false
		end
	end

	return result
end

local cap_original_c4baginteractionext_interactblocked = C4BagInteractionExt._interact_blocked
function C4BagInteractionExt:_interact_blocked(player)
	_override_has_special_equipment = true
	local blocked, skip_hint, custom_hint = cap_original_c4baginteractionext_interactblocked(self, player)
	_override_has_special_equipment = false

	if blocked then
		if CrewAbilityPockets:get_suitable_teamAI('c4', player:position(), false) then
			self.cap_given_to_bot = true
			blocked = false
		end
	end

	return blocked, skip_hint, custom_hint
end

local cap_original_c4baginteractionext_interact = C4BagInteractionExt.interact
function C4BagInteractionExt:interact(player)
	_given_to_bot = self.cap_given_to_bot
	self.cap_given_to_bot = false
	if _given_to_bot and alive(self._unit) then
		if Network:is_server() then
			if not CrewAbilityPockets:give_item_to_teamAI('c4', player:position(), true) then
				return
			end
		else
			managers.network:session():send_to_host('sync_interacted', self._unit, self._unit:id(), 'c4_stash_in_bot_pocket', 1)
		end
	end
	return cap_original_c4baginteractionext_interact(self, player)
end

local cap_original_multipleequipmentbaginteractionext_interactblocked = MultipleEquipmentBagInteractionExt._interact_blocked
function MultipleEquipmentBagInteractionExt:_interact_blocked(player)
	_override_has_special_equipment = true
	local blocked, skip_hint, custom_hint = cap_original_multipleequipmentbaginteractionext_interactblocked(self, player)
	_override_has_special_equipment = false

	if blocked then
		if CrewAbilityPockets:get_suitable_teamAI(self._special_equipment or 'c4', player:position(), false) then
			self.cap_given_to_bot = true
			blocked = false
		end
	end

	return blocked, skip_hint, custom_hint
end

local cap_original_multipleequipmentbaginteractionext_interact = MultipleEquipmentBagInteractionExt.interact
function MultipleEquipmentBagInteractionExt:interact(player)
	_given_to_bot = self.cap_given_to_bot
	self.cap_given_to_bot = false
	if _given_to_bot and alive(self._unit) then
		local equipment_name = self._special_equipment or 'c4'
		if Network:is_server() then
			if not CrewAbilityPockets:give_item_to_teamAI(equipment_name, player:position(), true) then
				return
			end
		else
			local max_player_can_carry = tweak_data.equipments.specials[equipment_name].quantity or 1
			managers.network:session():send_to_host('sync_interacted', self._unit, self._unit:id(), 'multi_stash_in_bot_pocket', max_player_can_carry)
		end
	end
	return cap_original_multipleequipmentbaginteractionext_interact(self, player)
end

local cap_original_multipleequipmentbaginteractionext_syncinteracted = MultipleEquipmentBagInteractionExt.sync_interacted
function MultipleEquipmentBagInteractionExt:sync_interacted(peer, player, amount_wanted)
	if amount_wanted == 0 and _given_to_bot then
		local equipment_name = self._special_equipment or 'c4'
		local max_player_can_carry = tweak_data.equipments.specials[equipment_name].quantity or 1
		amount_wanted = max_player_can_carry
	end

	cap_original_multipleequipmentbaginteractionext_syncinteracted(self, peer, player, amount_wanted)
end

local cap_original_specialequipmentinteractionext_interactblocked = SpecialEquipmentInteractionExt._interact_blocked
function SpecialEquipmentInteractionExt:_interact_blocked(player)
	local blocked, skip_hint, custom_hint = cap_original_specialequipmentinteractionext_interactblocked(self, player)

	if _from_interact_start and blocked then
		local special_equipment = self._special_equipment or self._tweak_data.special_equipment_block
		if CrewAbilityPockets:get_suitable_teamAI(special_equipment, player:position(), false) then
			self.cap_given_to_bot = true
			blocked = false
		end
	end

	return blocked, skip_hint, custom_hint
end

local cap_original_specialequipmentinteractionext_applyitempickup = SpecialEquipmentInteractionExt.apply_item_pickup
function SpecialEquipmentInteractionExt:apply_item_pickup()
	if not self.cap_given_to_bot then
		cap_original_specialequipmentinteractionext_applyitempickup(self)
	end
end

local cap_original_specialequipmentinteractionext_interact = SpecialEquipmentInteractionExt.interact
function SpecialEquipmentInteractionExt:interact(player)
	if self.cap_given_to_bot and alive(self._unit) then
		local special_equipment = self._special_equipment or self._tweak_data.special_equipment_block
		if Network:is_server() then
			if not CrewAbilityPockets:give_item_to_teamAI(special_equipment, player:position(), true) then
				self.cap_given_to_bot = false
			end
		else
			managers.network:session():send_to_host('sync_interacted', self._unit, self._unit:id(), 'stash_in_bot_pocket', 1)
		end
	end
	return cap_original_specialequipmentinteractionext_interact(self, player)
end

local cap_original_playermanager_hasspecialequipment = PlayerManager.has_special_equipment
function PlayerManager:has_special_equipment(name)
	local result = cap_original_playermanager_hasspecialequipment(self, name)

	if result and _override_has_special_equipment then
		if alive(self:player_unit()) and CrewAbilityPockets:get_suitable_teamAI(name, self:player_unit():position(), false) then
			_can_be_taken_by_bot = true
			result = nil
		end
	end

	return result
end

local cap_original_playermanager_addspecial = PlayerManager.add_special
function PlayerManager:add_special(params)
	if _given_to_bot then
		_given_to_bot = false
		return
	end

	return cap_original_playermanager_addspecial(self, params)
end

local cap_original_playermanager_removespecial = PlayerManager.remove_special
function PlayerManager:remove_special(...)
	cap_original_playermanager_removespecial(self, ...)
	CrewAbilityPockets:update_all_interactions()
end

tweak_data.interaction[CrewAbilityPockets.interaction_name] = {
	text_id = 'cap_hud_interact_take_from_teammate',
	interact_distance = 200,
	interaction_obj = Idstring('Spine2'),
	sound_event = 'money_grab',
	contour_preset = 'teammate',
	contour_preset_selected = 'teammate_downed_selected'
}

local cap_original_reviveinteractionext_isinrequiredstate = ReviveInteractionExt.set_active
function ReviveInteractionExt:set_active(active, sync, ...)
	if active and self.tweak_data == 'revive' and Network:is_server() then
		if self._unit:slot() == 16 then
			ReviveInteractionExt.super.set_active(self, active, true)
		end

	elseif self.tweak_data == CrewAbilityPockets.interaction_name then
		ReviveInteractionExt.super.set_active(self, active, false)
		return
	end

	cap_original_reviveinteractionext_isinrequiredstate(self, active, sync, ...)

	if not active and self.tweak_data == 'revive' then
		DelayedCalls:Add('DelayedPockets_setactive' .. tostring(self._unit:key()), 0, function()
			local character = managers.criminals:character_by_unit(self._unit)
			CrewAbilityPockets:update_interaction(character)
		end)
	end
end

local cap_original_reviveinteractionext_interact = ReviveInteractionExt.interact
function ReviveInteractionExt:interact(player)
	if self.tweak_data == CrewAbilityPockets.interaction_name then
		if alive(player) and player == managers.player:player_unit() then
			if self:can_interact(player) then
				if Network:is_server() then
					CrewAbilityPockets:transfer_items_from_teamAI_to_unit(self._unit, player, false)
				else
					local character = managers.criminals:character_by_unit(self._unit)
					LuaNetworking:SendToPeer(1, 'PocketTakeFrom', character.name)
				end
				if self._tweak_data.sound_event then
					player:sound():play(self._tweak_data.sound_event)
				end
			end
		end
		return
	end

	cap_original_reviveinteractionext_interact(self, player)
end
