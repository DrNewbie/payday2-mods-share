function TeamAIMovement:add_weapons()
	if Network:is_server() then
		local char_name = self._ext_base._tweak_table
		local loadout = managers.criminals:get_loadout_for(char_name)
		local crafted = managers.blackmarket:get_crafted_category_slot("primaries", loadout.primary_slot)

		if crafted then
			self._unit:inventory():add_unit_by_factory_blueprint_selection_index(loadout.primary, false, false, crafted.blueprint, crafted.cosmetics, 2)
		elseif loadout.primary then
			self._unit:inventory():add_unit_by_factory_name_selection_index(loadout.primary, false, false, nil, "", 2)
		else
			local weapon = self._ext_base:default_weapon_name("primary")
			local _ = weapon and self._unit:inventory():add_unit_by_factory_name_selection_index(weapon, false, false, nil, "", 2)
		end

		local sec_weap_name = self._ext_base:default_weapon_name("secondary")
		local _ = sec_weap_name and self._unit:inventory():add_unit_by_name_selection_index(sec_weap_name, false, 1)
	else
		TeamAIMovement.super.add_weapons(self)
	end
end