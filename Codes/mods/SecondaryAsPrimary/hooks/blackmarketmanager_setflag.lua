local primaries = tweak_data.gui.buy_weapon_categories.primaries
local secondaries = tweak_data.gui.buy_weapon_categories.secondaries

local function categoryContainsCheck( category_table, category_data )
	for _, actual_category_data in ipairs( category_table ) do
		local any_difference = false

		for _, category in ipairs( category_data ) do
			if actual_category_data[_] and ( category ~= actual_category_data[_] ) then
				any_difference = true
			end
		end

		if not any_difference then
			return false
		end
	end

	return true
end

SecondaryAsPrimaryGlobalStatus = true

for _, data in ipairs( secondaries ) do
	if categoryContainsCheck( primaries, data ) then
		table.insert( primaries, data )
	end
end

-- PrimaryAsSecondaryGlobalStatus = true

-- for _, data in ipairs( primaries ) do
-- 	if categoryContainsCheck( secondaries, data ) then
-- 		table.insert( secondaries, data )
-- 	end
-- end