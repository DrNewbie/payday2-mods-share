
if TwoClickSafeHouse_MenuManager_Hooked then
	return
else
	TwoClickSafeHouse_MenuManager_Hooked = true
end

if TwoClickSafeHouse_ModPath == nil then
	TwoClickSafeHouse_ModPath = ModPath
end

-- Based upon TdlQ's Lobby Player Info mod's localizations loading code
Hooks:Add("LocalizationManagerPostInit", "LocalizationManagerPostInit_TwoClickSafeHouse", function(loc)
	local commonpath = TwoClickSafeHouse_ModPath .. "localizations/"
	if _G.PD2KR then
		loc:load_localization_file(commonpath .. "korean.txt")
	else
		local GetFiles = _G.file.GetFiles
		local Idstring = _G.Idstring
		local activelanguagekey = SystemInfo:language():key()
		for __, filename in ipairs(GetFiles(commonpath)) do
			if Idstring(filename:match("^(.*).txt$") or ""):key() == activelanguagekey then
				loc:load_localization_file(commonpath .. filename)
				break
			end
		end
	end

	loc:load_localization_file(commonpath .. "english.txt", false)
end)

Hooks:Add("MenuManagerBuildCustomMenus", "BuildTwoClickSafeHouseMenu", function(menu_manager, nodes)
	local mainmenu = nodes.main
	if mainmenu == nil then
		-- Not actually a critical error since this occurs when hosting / joining a game session
--		log("[TwoClickSafeHouse] Fatal Error: Failed to locate main menu, aborting")
		return
	end
	if mainmenu._items == nil then
		log("[TwoClickSafeHouse] Fatal Error: Main menu node is empty, aborting")
		return
	end

	-- From Menu:AddButton() (mods/base/req/menus.lua)
	local data = {
		type = "CoreMenuItem.Item",
	}
	-- Archiving the translations in case OVK removes the strings in future:
	-- menu_safehouse -> Safe House
	-- menu_safehouse_help -> Go to the Safe House.
	local params = {
		name = "select_safehouse_btn",
		text_id = "menu_safehouse",
		help_id = "menu_safehouse_help",
		callback = "select_safehouse"
	}
	local new_item = mainmenu:create_item(data, params)
--	mainmenu:add_item(new_item)
	-- From MenuNode:add_item() (core/lib/managers/menu/coremenunode)
	new_item.dirty_callback = callback(mainmenu, mainmenu, "item_dirty")
	if mainmenu.callback_handler then
		new_item:set_callback_handler(mainmenu.callback_handler)
	end

	-- Determine where the item should be inserted
	local position = 8
	for index, item in pairs(mainmenu._items) do
		if item:name() == "divider_test2" then
			position = index
			break
		end
	end
	table.insert(mainmenu._items, position, new_item)
--	mainmenu:set_default_item_name("select_safehouse_btn")
end)

function MenuCallbackHandler:select_safehouse()
	local function StartU110SafeHouse()
		Global.game_settings.single_player = true
		local CustomSafehouseGuiPageMap = _G.CustomSafehouseGuiPageMap
		if CustomSafehouseGuiPageMap ~= nil and type(CustomSafehouseGuiPageMap.go_to_safehouse) == "function" then
			CustomSafehouseGuiPageMap:go_to_safehouse({skip_question = true})
		else
			log("[TwoClickSafeHouse] MenuCallbackHandler:select_safehouse() | Error: CustomSafehouseGuiPageMap is nil or CustomSafehouseGuiPageMap.go_to_safehouse is not a function, aborting")
		end
	end
	local function StartOldSafeHouse()
		self:play_safehouse({skip_question = true})
	end

	local localizationmanager = managers.localization
	managers.system_menu:show({
		focus_button = 1,
		title = localizationmanager:text("dialog_safehouse_title"),
		text = localizationmanager:text("dialog_safehouse_goto_text"),
		button_list = {
			{
				text = localizationmanager:text("menu_custom_safehouse"),
				callback_func = StartU110SafeHouse
			},
			{
				text = localizationmanager:text("twoclicksafehouse_oldsafehouse"),
				callback_func = StartOldSafeHouse
			},
			{
				cancel_button = true,
				text = localizationmanager:text("dialog_no")
			}
		}
	})
end
