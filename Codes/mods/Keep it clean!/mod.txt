{
	"blt_version" : 2,
	"name" : "Keep It Clean!",
	"description" : "Civilians are precious, cops will get angry if you hurt them!",
	"author" : "TdlQ",
	"image" : "tdlq.dds",
	"color" : "0.52 1 0",
	"contact" : "",
	"version" : "11",
	"simple_update_url" : "http://pd2mods.z77.fr/update/KeepItClean.zip",
	"hooks" : [
		{
			"hook_id" : "lib/managers/menumanager",
			"script_path" : "lua/menumanager.lua"
		},
		{
			"hook_id" : "lib/managers/group_ai_states/groupaistatebesiege",
			"script_path" : "lua/groupaistatebesiege.lua"
		},
		{
			"hook_id" : "lib/units/beings/player/huskplayermovement",
			"script_path" : "lua/huskplayermovement.lua"
		},
		{
			"hook_id" : "lib/units/beings/player/playermovement",
			"script_path" : "lua/playermovement.lua"
		},
		{
			"hook_id" : "lib/units/civilians/civiliandamage",
			"script_path" : "lua/civiliandamage.lua"
		},
		{
			"hook_id" : "lib/units/civilians/logics/civilianlogicinactive",
			"script_path" : "lua/civilianlogicinactive.lua"
		}
	]
}
