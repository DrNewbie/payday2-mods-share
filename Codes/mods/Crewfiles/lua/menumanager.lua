local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

_G.Crewfiles = _G.Crewfiles or {}
Crewfiles._path = ModPath
Crewfiles._data_path = SavePath .. 'Crewfiles.txt'
Crewfiles._steamed_data_path = SavePath .. 'Crewfiles_' .. Steam:userid() .. '.txt'
Crewfiles._import_menu_id = 'Crewfiles_links'
Crewfiles.settings = {
	links = {},
}

function Crewfiles:load()
	local fh = io.open(self._steamed_data_path, 'r') or io.open(self._data_path, 'r')
	if fh then
		for k, v in pairs(json.decode(fh:read('*all')) or {}) do
			self.settings[k] = v
		end
		fh:close()
	end
	self.settings.data = nil
end

function Crewfiles:save()
	local fh = io.open(self._steamed_data_path, 'w+')
	if fh then
		fh:write(json.encode(self.settings))
		fh:close()
	end
end

function Crewfiles:set_linked_profile_index(index, link_to_index)
	self.settings.links[index] = link_to_index
end

function Crewfiles:get_linked_profile_index(index)
	return self.settings.links[index]
end

function Crewfiles.get_profile_name(profile, index)
	index = index or '?'
	local name
	if profile.name then
		name = profile.name
	elseif profile.perk_deck then
		name = ('Profile %s (%s)'):format(index, managers.localization:text('menu_st_spec_' .. profile.perk_deck))
	elseif index then
		name = 'Profile ' .. index
	end
	return name
end

function Crewfiles.modify_node(node)
	node:clean_items()
	local profiles_nr = managers.multi_profile:profile_count()
	for i = 1, profiles_nr do
		local data = {
			type = 'MenuItemMultiChoice'
		}
		for j = 1, profiles_nr do
			local name = j == i and '-' or Crewfiles.get_profile_name(managers.multi_profile:profile(j), j)
			local choice = {_meta = 'option', text_id = name, value = j, localize = false, selected = j == i}
			table.insert(data, choice)
		end

		local params = {
			name = 'button_Crewfiles_link-' .. tostring(i),
			text_id = Crewfiles.get_profile_name(managers.multi_profile:profile(i), i),
			callback = 'CrewfilesHandleHub',
			to_upper = false,
			help_id = managers.localization:text('cf_options_item_desc'),
			localize = false,
			localize_help = false,
			priority = i
		}

		local new_item = node:create_item(data, params)
		new_item:set_value(Crewfiles.settings.links[i])
		node:add_item(new_item)
	end
	managers.menu:add_back_button(node)
	return node
end

MenuCallbackHandler.CrewfilesHandleHub = function(this, item)
	Crewfiles:set_linked_profile_index(item._parameters.priority, item._current_index)
	if item._parameters.priority == Global.multi_profile._current_profile then
		managers.multi_profile:load_current()
	end
end

MenuCallbackHandler.CrewfilesBackCallback = function(node, focus)
	managers.multi_profile:load_current()
	Crewfiles:save()
end

Hooks:Add('MenuManagerSetupCustomMenus', 'MenuManagerSetupCustomMenus_Crewfiles', function(menu_manager, nodes)
	MenuHelper:NewMenu(Crewfiles._import_menu_id)
end)

Hooks:Add('MenuManagerBuildCustomMenus', 'MenuManagerBuildCustomMenus_Crewfiles', function(menu_manager, nodes)
	local menu = MenuHelper:BuildMenu(Crewfiles._import_menu_id, {})
	menu:parameters().modifier = {Crewfiles.modify_node}
	menu:parameters().back_callback = {MenuCallbackHandler.CrewfilesBackCallback}
	nodes[Crewfiles._import_menu_id] = menu

	MenuHelper:AddMenuItem(nodes['blt_options'], Crewfiles._import_menu_id, 'cf_options_menu_title', 'cf_options_menu_desc')
end)

Hooks:Add('LocalizationManagerPostInit', 'LocalizationManagerPostInit_Crewfiles', function(loc)
	local language_filename

	for _, filename in pairs(file.GetFiles(Crewfiles._path .. 'loc/')) do
		local str = filename:match('^(.*).txt$')
		if str and Idstring(str) and Idstring(str):key() == SystemInfo:language():key() then
			language_filename = filename
			break
		end
	end

	if language_filename then
		loc:load_localization_file(Crewfiles._path .. 'loc/' .. language_filename)
	end
	loc:load_localization_file(Crewfiles._path .. 'loc/english.txt', false)
end)

Crewfiles:load()
