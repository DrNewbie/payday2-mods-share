local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

local cf_original_crewmanagementgui_init = CrewManagementGui.init
function CrewManagementGui:init(ws, fullscreen_ws, node)
	managers.multi_profile:save_current()
	Crewfiles:save()

	cf_original_crewmanagementgui_init(self, ws, fullscreen_ws, node)

	local x = self._1_panel:left()
	self._multi_profile_item = CfMultiProfileItemGui:new(self._ws, self._panel, x)
end

local cf_original_crewmanagementgui_reload = CrewManagementGui.reload
function CrewManagementGui:reload()
	cf_original_crewmanagementgui_reload(self)

	managers.multi_profile:save_current()
	Crewfiles:save()
end

local cf_original_crewmanagementgui_mousemoved = CrewManagementGui.mouse_moved
function CrewManagementGui:mouse_moved(o, x, y)
	local used, pointer = cf_original_crewmanagementgui_mousemoved(self, o, x, y)

	local u, p = self._multi_profile_item:mouse_moved(x, y)
	used = u or used
	pointer = p or pointer

	return used, pointer
end

local cf_original_crewmanagementgui_mousepressed = CrewManagementGui.mouse_pressed
function CrewManagementGui:mouse_pressed(button, x, y)
	cf_original_crewmanagementgui_mousepressed(self, button, x, y)
	self._multi_profile_item:mouse_pressed(button, x, y)
end

local function make_fine_text(text)
	local x, y, w, h = text:text_rect()
	text:set_size(w, h)
	text:set_position(math.round(text:x()), math.round(text:y()))
end

_G.CfMultiProfileItemGui = _G.CfMultiProfileItemGui or class(MultiProfileItemGui)

function CfMultiProfileItemGui:init(ws, panel, x)
	CfMultiProfileItemGui.super.init(self, ws, panel)

	self:set_name_editing_enabled(false)
	self._profile_panel:child('arrow_left'):set_x(100000)
	self._profile_panel:child('arrow_right'):set_x(100000)

	self._panel:set_bottom(619)
	self._panel:set_right(x - 20)

	local link_text = panel:text({
		text = managers.localization:to_upper_text('cf_link_profile'),
		font = tweak_data.menu.pd2_medium_font,
		font_size = tweak_data.menu.pd2_medium_font_size,
	})
	make_fine_text(link_text)
	link_text:set_center_y(self._panel:center_y())
	link_text:set_right(self._panel:left() - 4)
end

function CfMultiProfileItemGui:update()
	local mp = managers.multi_profile
	local original_current_profile_name = mp.current_profile_name
	mp.current_profile_name = mp.cf_current_profile_name
	local original_has_previous = mp.has_previous
	mp.has_previous = mp.cf_has_previous
	local original_has_next = mp.has_next
	mp.has_next = mp.cf_has_next

	CfMultiProfileItemGui.super.update(self)

	mp.current_profile_name = original_current_profile_name
	mp.has_previous = original_has_previous
	mp.has_next = original_has_next
end

function CfMultiProfileItemGui:mouse_pressed(button, x, y)
	if button == Idstring('0') then
		if self._name_text:inside(x, y) or self:arrow_selection() == 'quick' then
			managers.multi_profile:cf_open_quick_select()
			managers.menu_component:post_event('menu_enter')
			return
		end
	end
end
