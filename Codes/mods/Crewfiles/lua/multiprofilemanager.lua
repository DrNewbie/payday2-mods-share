local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

local cf_original_multiprofilemanager_checkamount = MultiProfileManager._check_amount
function MultiProfileManager:_check_amount()
	cf_original_multiprofilemanager_checkamount(self)

	local changed
	for i = 1, self:profile_count() do
		if not Crewfiles.settings.links[i] then
			Crewfiles.settings.links[i] = i
			changed = true
		end
	end
	if changed then
		Crewfiles:save()
	end
end

local cf_original_multiprofilemanager_loadcurrent = MultiProfileManager.load_current
function MultiProfileManager:load_current()
	local profile = self:current_profile()
	local original_henchmen_loadout = profile.henchmen_loadout
	local original_preferred_henchmen = profile.preferred_henchmen

	local used_index = Crewfiles:get_linked_profile_index(self._global._current_profile)
	if used_index ~= self._global._current_profile then
		local used_profile = self._global._profiles[used_index]
		profile.henchmen_loadout = used_profile and used_profile.henchmen_loadout or {}
		profile.preferred_henchmen = used_profile and used_profile.preferred_henchmen or {}
	end

	cf_original_multiprofilemanager_loadcurrent(self)

	-- restore random slot
	if profile.preferred_henchmen then
		for i = 1, CriminalsManager.MAX_NR_TEAM_AI do
			if not profile.preferred_henchmen[i] then
				managers.blackmarket:set_preferred_henchmen(i)
			end
		end
	end

	profile.henchmen_loadout = original_henchmen_loadout
	profile.preferred_henchmen = original_preferred_henchmen
end

local cf_original_multiprofilemanager_savecurrent = MultiProfileManager.save_current
function MultiProfileManager:save_current()
	local profile = self:current_profile()
	local original_henchmen_loadout = profile and profile.henchmen_loadout or {}
	local original_preferred_henchmen = profile and profile.preferred_henchmen or {}

	cf_original_multiprofilemanager_savecurrent(self)

	local used_index = Crewfiles:get_linked_profile_index(self._global._current_profile)
	if used_index ~= self._global._current_profile then
		profile = self:current_profile()
		local used_profile = self._global._profiles[used_index]
		if used_profile then
			used_profile.henchmen_loadout = profile.henchmen_loadout
			used_profile.preferred_henchmen = profile.preferred_henchmen
		end

		profile.henchmen_loadout = original_henchmen_loadout
		profile.preferred_henchmen = original_preferred_henchmen
	end
end

function MultiProfileManager:cf_set_current_profile(index)
	Crewfiles:set_linked_profile_index(self._global._current_profile, index)
	managers.multi_profile:load_current()
	local mcm = managers.menu_component
	local node = mcm._crew_management_gui._node
	mcm:close_crew_management_gui()
	mcm:create_crew_management_gui(node)
end

function MultiProfileManager:cf_current_profile()
	return self:profile(Crewfiles:get_linked_profile_index(self._global._current_profile))
end

function MultiProfileManager:cf_current_profile_name()
	local used_index = Crewfiles:get_linked_profile_index(self._global._current_profile)
	local profile = self:profile(used_index)

	if not profile then
		return 'Error'
	end

	return profile.name or ('Profile ' .. tostring(used_index))
end

function MultiProfileManager:cf_open_quick_select()
	local dialog_data = {
		title = '',
		text = '',
		button_list = {}
	}

	for idx, profile in pairs(self._global._profiles) do
		local text = profile.name or ('Profile ' .. idx)
		if Crewfiles.settings.links[self._global._current_profile] == idx then
			if idx == self._global._current_profile then
				text = '=== ' .. text
			else
				text = '<<< ' .. text
			end
		elseif idx == self._global._current_profile then
			text = '>>> ' .. text
		end
		table.insert(dialog_data.button_list, {
			text = text,
			callback_func = function ()
				self:cf_set_current_profile(idx)
			end,
			focus_callback_func = function ()
			end
		})
	end

	local divider = {
		no_text = true,
		no_selection = true
	}
	table.insert(dialog_data.button_list, divider)

	local no_button = {
		text = managers.localization:text('dialog_cancel'),
		focus_callback_func = function ()
		end,
		cancel_button = true
	}
	table.insert(dialog_data.button_list, no_button)

	dialog_data.image_blend_mode = 'normal'
	dialog_data.text_blend_mode = 'add'
	dialog_data.use_text_formating = true
	dialog_data.w = 480
	dialog_data.h = 532
	dialog_data.title_font = tweak_data.menu.pd2_medium_font
	dialog_data.title_font_size = tweak_data.menu.pd2_medium_font_size
	dialog_data.font = tweak_data.menu.pd2_small_font
	dialog_data.font_size = tweak_data.menu.pd2_small_font_size
	dialog_data.text_formating_color = Color.white
	dialog_data.text_formating_color_table = {}
	dialog_data.clamp_to_screen = true

	managers.system_menu:show_buttons(dialog_data)
end
