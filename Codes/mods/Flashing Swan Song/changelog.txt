Revision 17:
- changed update system to Simple Mod Updater

Revision 16:
- even more absolutely essential BLT2 integration

Revision 15:
- more absolutely essential BLT2 integration

Revision 14:
- BLT2

Revision 13:
- fixed flashing not started if entered swan song state while carrying a bag

Revision 12:
- fixed JSON errors

Revision 11:
- changed the method to remove the flashing contour

Revision 10:
- fixed the bug where a client exiting swan song kept his flashing contour (host was not affected, only the other clients)

Revision 9:
- update 105.2 introduced new stuff which should get rid of the rare false positives

Revision 8:
- updated to U96 (bis)

Revision 7:
- updated to U96

Revision 6:
- fix _send_set_health() so it does not send 0 when it's not really 0

Revision 5:
- fix unstopped blinking when health at very low level is restored before armor

Revision 4:
- fix the non discontinuation of the flashing when the player is in custody

Revision 3:
- fix case where 1 health point gets rounded to 0% and triggers the effect
