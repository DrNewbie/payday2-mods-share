Revision 8:
- fixed issue with night vision masks (CG not disabled when downed)

Revision 7:
- fixed xml tweaks always rebuilt

Revision 6:
- fixed special option value "default" that was not saved/usable

Revision 5:
- changed how the default CG is stored to prevent any corruption to last till the end of the heist

Revision 4:
- fixed default color grading overridden when going in custody
- added option for special color grading while down

Revision 3:
- fixed default color grading overridden when armor is set to 0 two consecutive times

Revision 2:
- added support for Stoic delayed damages
- added 24 new color grading settings

Revision 1:
- initial release