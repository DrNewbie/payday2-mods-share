local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

_G.NoArmorColorGrading = _G.NoArmorColorGrading or {}
NoArmorColorGrading._path = ModPath
NoArmorColorGrading._data_path = SavePath .. 'no_armor_color_grading.txt'
NoArmorColorGrading.settings = {
	on_no_armor = 'color_sin_classic',
	on_delayed_damage = 'color_TdlQ_colorless',
	on_down = 'color_TdlQ_washed_out',
}

function NoArmorColorGrading:load()
	local fh = io.open(self._data_path, 'r')
	if fh then
		for k, v in pairs(json.decode(fh:read('*all'))) do
			self.settings[k] = v
		end
		fh:close()
	end
end

function NoArmorColorGrading:save()
	local fh = io.open(self._data_path, 'w+')
	if fh then
		fh:write(json.encode(self.settings))
		fh:close()
	end
end

local function _extract_fine_name(fullname)
	return (fullname:gsub('[.].-$', ''):gsub('^[0-9]-_', ''))
end

function NoArmorColorGrading:load_texture(name)
	local texture = 'core/textures/cg_TdlQ_' .. _extract_fine_name(name)
	DB:create_entry(
		Idstring('texture'),
		Idstring(texture),
		self._path .. 'assets/' .. name
	)
	Application:reload_textures( { Idstring(texture) } )
end

function NoArmorColorGrading:setup(loc)
	local tweaks = self._path .. 'xml/tweaks.xml'
	if SystemFS:exists(tweaks) and file.FileHash(tweaks) == 'b6ada4778f137456a34ab61c68a2e1e57f5df23e97eb2743c7b2088952edc1dd' then
		-- qued
	else
		self:build_tweaks(tweaks)
	end

	table.insert(tweak_data.color_grading, { value = 'color_sin', text_id = 'menu_pp_asset_pex_camera_access' })

	local subfolder = 'assets/'
	local path = self._path .. subfolder
	local files = SystemFS:list(path)
	local strs = {}
	for i, name in ipairs(files) do
		self:load_texture(name)
		local name_no_ext = _extract_fine_name(name)
		local text_id = 'cg_TdlQ_' .. name_no_ext
		table.insert(tweak_data.color_grading, { value = 'color_TdlQ_' .. name_no_ext, text_id = text_id })
		strs[text_id] = name_no_ext:gsub('_', ' ') .. ' [TdlQ]'
	end
	loc:add_localized_strings(strs)
end

function NoArmorColorGrading:build_tweaks(filename)
	local xml = '<?xml version="1.0"?><tweaks><tweak version="2" name="core/shaders/post_processor" extension="post_processor"><search><?xml version="1.0" encoding="utf-8" ?><post_processor /><materials /></search><target mode="attach">\n'

	local subfolder = 'assets/'
	local path = self._path .. subfolder
	local files = SystemFS:list(path)
	for i, name in ipairs(files) do
		local name_no_ext = _extract_fine_name(name)
		xml = xml .. '<material name="cg_TdlQ_' .. name_no_ext .. '"><filter_color_texture file="core/textures/cg_TdlQ_' .. name_no_ext .. '"/><variable name="LUT_settings_a" type="vector3" value="0 0 0"/><variable name="LUT_settings_b" type="vector3" value="0 0 0"/><variable name="contrast" type="scalar" value="0"/><reflection_texture file="core/textures/cg_he"/></material>\n'
	end

	xml = xml .. '</target></tweak><tweak version="2" name="core/shaders/post_processor" extension="post_processor"><search><?xml version="1.0" encoding="utf-8" ?><post_processor /></search><target mode="attach">\n'

	for i, name in ipairs(files) do
		local name_no_ext = _extract_fine_name(name)
		xml = xml .. '<effect name="color_TdlQ_' .. name_no_ext .. '"><combiner src_respect_viewport="false" dst_respect_viewport="false" diffuse_texture="low_target_6" dst="lut_target" render_template="post_color_grading_prepare" material="cg_TdlQ_' .. name_no_ext .. '" scriptable="lut_settings"/><combiner diffuse_texture="bb" self_illumination_texture="lut_target" dst="back_buffer" render_template="post_color_grading" material="cg_TdlQ_' .. name_no_ext .. '" scriptable="lut_settings"/></effect>\n'
	end

	xml = xml .. '</target></tweak></tweaks>'

	local fh = io.open(filename, 'w+')
	if fh then
		fh:write(xml)
		fh:close()
		log('[NACG] XML tweaks rebuilt: ' .. file.FileHash(filename))
	end
end

Hooks:Add('LocalizationManagerPostInit', 'LocalizationManagerPostInit_NoArmorColorGrading', function(loc)
	for _, filename in pairs(file.GetFiles(NoArmorColorGrading._path .. 'loc/')) do
		local str = filename:match('^(.*).txt$')
		if str and Idstring(str) and Idstring(str):key() == SystemInfo:language():key() then
			loc:load_localization_file(NoArmorColorGrading._path .. 'loc/' .. filename)
			break
		end
	end
	loc:load_localization_file(NoArmorColorGrading._path .. 'loc/english.txt', false)

	NoArmorColorGrading:setup(loc)
	NoArmorColorGrading:load()
end)

Hooks:Add('MenuManagerInitialize', 'MenuManagerInitialize_NoArmorColorGrading', function(menu_manager)
	function MenuCallbackHandler:NoArmorColorGradingMultipleChoice(item)
		NoArmorColorGrading.settings[item:name()] = item:value()
	end

	function MenuCallbackHandler:NoArmorColorGradingChangedFocus(focus)
		if not focus then
			NoArmorColorGrading:save()
		end
	end

	MenuHelper:LoadFromJsonFile(NoArmorColorGrading._path .. 'menu/options.txt', NoArmorColorGrading, NoArmorColorGrading.settings)

	Hooks:Add('MenuManagerBuildCustomMenus', 'MenuManagerBuildCustomMenus_NoArmorColorGrading', function(menu_manager, nodes)
		nodes.nacg_options_menu:parameters().modifier = {NoArmorColorGradingCreator.modify_node}
	end)
end)

local function _make_multichoice(node, setting_name, text_id, help_id)
	local index = 1
	local data = { type = 'MenuItemMultiChoice' }
	for i, cg in ipairs(tweak_data.color_grading) do
		local value = cg.value or false
		table.insert(data, { _meta = 'option', text_id = cg.text_id, value = value })
		if value == NoArmorColorGrading.settings[setting_name] then
			index = i
		end
	end

	local params = {
		name = setting_name,
		text_id = text_id,
		help_id = help_id,
		callback = 'NoArmorColorGradingMultipleChoice',
		_to_upper = false,
		localize = true,
		localize_help = true,
	}

	local new_item = node:create_item(data, params)
	new_item._current_index = index or new_item._current_index

	return new_item
end

_G.NoArmorColorGradingCreator = _G.NoArmorColorGradingCreator or class()
function NoArmorColorGradingCreator.modify_node(node)
	local old_items = node:items()

	node:clean_items()

	node:add_item(_make_multichoice(node, 'on_no_armor', 'nacg_options_no_armor_title', 'nacg_options_no_armor_desc'))
	node:add_item(_make_multichoice(node, 'on_delayed_damage', 'nacg_options_stoic_delayed_title', 'nacg_options_stoic_delayed_desc'))
	node:add_item(_make_multichoice(node, 'on_down', 'nacg_options_down_title', 'nacg_options_down_desc'))

	managers.menu:add_back_button(node)

	return node
end
