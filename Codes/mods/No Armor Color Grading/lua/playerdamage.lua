local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

local mode = 0
local default_video_color_grading

local function _update_video_color_grading()
	default_video_color_grading = managers.user:get_setting('video_color_grading')
end

local nacg_original_playerdamage_init = PlayerDamage.init
function PlayerDamage:init(...)
	nacg_original_playerdamage_init(self, ...)

	_update_video_color_grading()
	managers.user:add_setting_changed_callback('video_color_grading', _update_video_color_grading)

	local mask_id = managers.blackmarket:equipped_mask().mask_id
	local mask_tweak = tweak_data.blackmarket.masks[mask_id]
	local night_vision = mask_tweak.night_vision
	if not night_vision then
		if managers.player:has_category_upgrade('player', 'armor_to_health_conversion') then
			mode = 2
		else
			mode = 1
		end
	end
end

function PlayerDamage:nacg_override_color_grading(value)
	if not value then
		return
	end

	self.nacg_overridden_color_grading = true
	managers.environment_controller:set_default_color_grading(value, true)
	managers.environment_controller:refresh_render_settings()
end

function PlayerDamage:nacg_restore_color_grading()
	managers.environment_controller:set_default_color_grading(default_video_color_grading)
	managers.environment_controller:refresh_render_settings()
	self.nacg_overridden_color_grading = false
end

local nacg_original_playerdamage_setarmor = PlayerDamage.set_armor
function PlayerDamage:set_armor(armor)
	nacg_original_playerdamage_setarmor(self, armor)

	if mode == 1 then
		if armor <= 0 then
			if not self.nacg_overridden_color_grading then
				self:nacg_override_color_grading(NoArmorColorGrading.settings.on_no_armor)
			end
		elseif self.nacg_overridden_color_grading then
			self:nacg_restore_color_grading()
		end
	end
end

local nacg_original_playerdamage_delaydamage = PlayerDamage.delay_damage
function PlayerDamage:delay_damage(...)
	nacg_original_playerdamage_delaydamage(self, ...)

	if mode == 2 and not self.nacg_overridden_color_grading then
		self:nacg_override_color_grading(NoArmorColorGrading.settings.on_delayed_damage)
	end
end

local nacg_original_playerdamage_cleardelayeddamage = PlayerDamage.clear_delayed_damage
function PlayerDamage:clear_delayed_damage()
	if mode == 2 and self.nacg_overridden_color_grading then
		self:nacg_restore_color_grading()
	end

	return nacg_original_playerdamage_cleardelayeddamage(self)
end

local nacg_original_playerdamage_removeondamageevent = PlayerDamage._remove_on_damage_event
function PlayerDamage:_remove_on_damage_event()
	if self.nacg_overridden_color_grading then
		self:nacg_restore_color_grading()
	end

	nacg_original_playerdamage_removeondamageevent(self)
end

local nacg_original_playerdamage_onenterbleedoutevent = PlayerDamage._on_enter_bleedout_event
function PlayerDamage:_on_enter_bleedout_event()
	nacg_original_playerdamage_onenterbleedoutevent(self)

	if mode > 0 then
		if self.nacg_overridden_color_grading then
			self:nacg_restore_color_grading()
		end
		self:nacg_override_color_grading(NoArmorColorGrading.settings.on_down)
	end
end

local nacg_original_playerdamage_predestroy = PlayerDamage.pre_destroy
function PlayerDamage:pre_destroy()
	if self.nacg_overridden_color_grading then
		self:nacg_restore_color_grading()
	end

	nacg_original_playerdamage_predestroy(self)
end
