{
	"blt_version" : 2,
	"name" : "No Armor Color Grading",
	"description" : "",
	"author" : "TdlQ",
	"image" : "tdlq.dds",
	"color" : "0.52 1 0",
	"version" : "8",
	"simple_update_url" : "http://pd2mods.z77.fr/update/NoArmorColorGrading.zip",
	"updates": [
		{
			"identifier" : "SimpleModUpdater",
			"display_name" : "Simple Mod Updater",
			"install_folder" : "Simple Mod Updater",
			"host" : { "meta": "http://pd2mods.z77.fr/meta/SimpleModUpdater", }
		}
	],
	"hooks" : [
		{
			"hook_id" : "lib/managers/menumanager",
			"script_path" : "lua/menumanager.lua"
		},
		{
			"hook_id" : "lib/units/beings/player/playerdamage",
			"script_path" : "lua/playerdamage.lua"
		}
	]
}
