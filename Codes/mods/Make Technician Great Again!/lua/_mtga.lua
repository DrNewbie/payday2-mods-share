_G.MTGA = _G.MTGA or {
	users = {},
	is_enabled = true,
	use_drill = false,
	interaction_name = 'mtga_smack_tank',
	smack_network_id = 'MTGA_smack',
	ids_drill = Idstring('units/payday2/equipment/item_door_drill_small/item_door_drill_small'),
	ids_c4 = Idstring('units/payday2/equipment/gen_equipment_tripmine/gen_equipment_tripmine'),
}

function MTGA.level_has_drill(level_id)
	local level_has_drill = {
		alex_2 = true,
		arm_cro = true,
		arm_fac = true,
		arm_for = true,
		arm_hcm = true,
		arm_par = true,
		arm_und = true,
		bex = true,
		big = true,
		born = true,
		bph = true,
		branchbank = true,
		cane = true,
		chas = true,
		chew = true,
		chill_combat = true,
		crojob2 = true,
		crojob3 = true,
		dark = true,
		dinner = true,
		election_day_1 = true,
		election_day_2 = true,
		election_day_3_skip1 = true,
		family = true,
		fish = true, -- stealth only
		firestarter_1 = true,
		firestarter_2 = true,
		firestarter_3 = true,
		four_stores = true,
		framing_frame_1 = true,
		framing_frame_3 = true,
		gallery = true,
		glace = true,
		help = true,
		hox_3 = true,
		jewelry_store = true,
		kenaz = true,
		kosugi = true,
		mad = true,
		mallcrasher = true,
		mex = true,
		mex_cooking = true,
		mia_1 = true,
		mia_2 = true,
		moon = true,
		mus = true,
		nail = true,
		nightclub = true,
		peta2 = true,
		pex = true,
		pbr2 = true,
		red2 = true,
		roberts = true,
		shoutout_raid = true,
		spa = true,
		tag = true, -- stealth only
		ukrainian_job = true,
		vit = true,
		watchdogs_1 = true,
		welcome_to_the_jungle_1 = true,
		welcome_to_the_jungle_2 = true,
	}
	level_id = level_id:gsub('_night$', ''):gsub('_day$', '')
	return level_has_drill[level_id]
end

function MTGA:get_drill_availability()
	if not self.users[1] then
		return false
	end

	if self.level_has_drill(Global.game_settings.level_id) then
		return true
	end

	for peer_id, state in pairs(self.users) do
		if state == false then
			return false
		end
	end

	return PackageManager:has(Idstring('unit'), self.ids_drill)
end

function MTGA:update_drill_availability()
	self.use_drill = self:get_drill_availability()
end

local tmp_vec = Vector3()
local mvec3_set = mvector3.set
local ids_head = Idstring('Head')

function MTGA:link_smacker_to_bulldo(parent_unit, smacker_unit)
	local using_drill
	if alive(smacker_unit) then
		using_drill = smacker_unit:name() == MTGA.ids_drill
	else
		using_drill = self.use_drill and math.random() > 0.3
	end

	local pos
	local rot = Rotation()
	local head = parent_unit:get_object(ids_head)
	local head_rot = head:rotation()
	if using_drill then
		mrotation.set_yaw_pitch_roll(rot, head_rot:yaw(), head_rot:pitch() - 130, head_rot:roll())
		pos = head:position() + head_rot:y() * 7 + head_rot:z() * 24
	else
		mrotation.set_yaw_pitch_roll(rot, head_rot:yaw(), head_rot:pitch() + 60, head_rot:roll())
		pos = head:position() + head_rot:y() * 7 + head_rot:z() * 20
	end

	if not smacker_unit then
		if using_drill then
			smacker_unit = World:spawn_unit(self.ids_drill, pos, rot)
			smacker_unit:timer_gui():set_can_jam(false)
			smacker_unit:timer_gui():set_override_timer(6)
			smacker_unit:base()._disable_upgrades = true
			smacker_unit:interaction():interact()
			smacker_unit:interaction():set_active(false, true)
		else
			smacker_unit = World:spawn_unit(self.ids_c4, pos, rot)
			smacker_unit:interaction():set_active(false, true)
		end

		for peer_id, state in pairs(self.users) do
			if state and peer_id ~= 1 then
				local peer = managers.network:session():peer(peer_id)
				if peer and peer:synched() then
					managers.network:session():send_to_peer(peer, 'loot_link', smacker_unit, parent_unit)
				end
			end
		end

	else
		smacker_unit:base()._disable_upgrades = true
		DelayedCalls:Add('DelayedModMTGA_drillpos_' .. tostring(parent_unit:key()), 0.01, function()
			if alive(smacker_unit) then
				smacker_unit:set_position(pos)
				smacker_unit:set_rotation(rot)
			end
		end)
	end

	parent_unit:interaction().mtga_in_progress = true
	parent_unit:link(ids_head, smacker_unit)

	return smacker_unit
end

function MTGA:unlink_smacker(smacker_unit)
	local bulldo = smacker_unit:parent()
	smacker_unit:unlink()

	if smacker_unit:name() == MTGA.ids_drill then
		smacker_unit:base():set_powered(false)

		local body = smacker_unit:body(0)
		body:set_enabled(true)
		body:set_dynamic()

		mvec3_set(tmp_vec, bulldo:movement():m_head_rot():y() * -1)
		smacker_unit:push(50, tmp_vec * -600)
	end

	if Network:is_server() then
		for peer_id, state in pairs(self.users) do
			if state and peer_id ~= 1 then
				local peer = managers.network:session():peer(peer_id)
				if peer and peer:synched() then
					managers.network:session():send_to_peer(peer, 'loot_link', smacker_unit, smacker_unit)
				end
			end
		end
	end
end
