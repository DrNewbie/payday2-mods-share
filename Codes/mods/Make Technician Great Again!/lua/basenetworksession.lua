local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

function BaseNetworkSession:mtga_send_to_peers_synched(messageType, type_index, enabled, tweak_data_id, ...)
	for peer_id, peer in pairs(self._peers) do
		peer:send_queued_sync(messageType, type_index, enabled, MTGA.users[peer_id] and tweak_data_id or 'drill', ...)
	end
end

Hooks:Add('NetworkReceivedData', 'NetworkReceivedData_MTGA', function(sender, messageType, data)
	if messageType == MTGA.smack_network_id then
		local u_id = tonumber(data)
		for ukey, record in pairs(managers.groupai:state()._police) do
			local unit = record.unit
			if alive(unit) and unit:id() == u_id then
				unit:interaction():mtga_start(managers.criminals:character_unit_by_peer_id(sender))
				break
			end
		end

	elseif messageType == 'MTGA?' then
		MTGA.users[sender] = true
		MTGA:update_drill_availability()
		LuaNetworking:SendToPeer(sender, 'MTGA!', '')

	elseif messageType == 'MTGA!' then
		MTGA.users[sender] = true
		MTGA:update_drill_availability()
	end
end)

Hooks:Add('BaseNetworkSessionOnLoadComplete', 'BaseNetworkSessionOnLoadComplete_MTGA', function(local_peer, id)
	MTGA.users[id] = true
	MTGA:update_drill_availability()
	LuaNetworking:SendToPeers('MTGA?', '')
end)

Hooks:Add('NetworkManagerOnPeerAdded', 'NetworkManagerOnPeerAdded_MTGA', function(peer, peer_id)
	MTGA.users[peer_id] = false
	MTGA:update_drill_availability()
end)

Hooks:Add('BaseNetworkSessionOnPeerRemoved', 'BaseNetworkSessionOnPeerRemoved_MTGA', function(peer, peer_id, reason)
	MTGA.users[peer_id] = nil
	MTGA:update_drill_availability()
end)
