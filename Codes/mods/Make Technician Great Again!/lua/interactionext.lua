local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

local ids_bullet_hit_blood = Idstring('effects/payday2/particles/impacts/blood/blood_impact_a')
local tmp_vec = Vector3()
local mvec3_set = mvector3.set

local mtga_original_intimitateinteractionext_init = IntimitateInteractionExt.init
function IntimitateInteractionExt:init(unit, ...)
	if alive(unit) then
		local ubase = unit:base()
		if ubase and ubase.has_tag and ubase:has_tag('tank') then
			self.is_tank = true
			self.tweak_data = MTGA.interaction_name
		end
	end
	mtga_original_intimitateinteractionext_init(self, unit, ...)
end

function IntimitateInteractionExt:save(data)
	IntimitateInteractionExt.super.save(self, data)
	if self.is_tank then
		data.InteractionExt.tweak_data = 'drill'
	end
end

function IntimitateInteractionExt:can_select(...)
	if self.is_tank then
		if self._unit:id() == -1 then
			return false
		end
		if self.mtga_in_progress then
			return false
		end
		if self.tweak_data ~= MTGA.interaction_name then
			self:set_tweak_data(MTGA.interaction_name)
		end
	end
	return IntimitateInteractionExt.super.can_select(self, ...)
end

local mtga_original_intimitateinteractionext_caninteract = IntimitateInteractionExt.can_interact
function IntimitateInteractionExt:can_interact(...)
	if self.is_tank then
		return true
	end
	return IntimitateInteractionExt.super.can_interact(self, ...)
end

function IntimitateInteractionExt:mtga_break_plate()
	mvec3_set(tmp_vec, self._unit:movement():m_head_rot():y() * -1)
	local body = self._unit:body('body_helmet_plate')
	local col_ray =	{
		body = body,
		unit = self._unit,
		ray = tmp_vec,
		normal = tmp_vec,
		position = self.mtga_smacker:position()
	}
	local normal_vec_yaw, normal_vec_pitch = InstantBulletBase._get_vector_sync_yaw_pitch(col_ray.normal, 128, 64)
	local dir_vec_yaw, dir_vec_pitch = InstantBulletBase._get_vector_sync_yaw_pitch(col_ray.ray, 128, 64)
	managers.network:session():send_to_peers_synched('sync_body_damage_bullet', col_ray.body, self.mtga_smacker, normal_vec_yaw, normal_vec_pitch, col_ray.position, dir_vec_yaw, dir_vec_pitch, 16384)

	self._unit:damage():run_sequence_simple('int_seq_helmet_plate')
end

function IntimitateInteractionExt:mtga_break_glass()
	mvec3_set(tmp_vec, self._unit:movement():m_head_rot():y() * -1)
	local body = self._unit:body('body_helmet_glass')
	local col_ray = {
		body = body,
		unit = self._unit,
		ray = tmp_vec,
		normal = tmp_vec,
		position = self.mtga_smacker:position()
	}
	local normal_vec_yaw, normal_vec_pitch = InstantBulletBase._get_vector_sync_yaw_pitch(col_ray.normal, 128, 64)
	local dir_vec_yaw, dir_vec_pitch = InstantBulletBase._get_vector_sync_yaw_pitch(col_ray.ray, 128, 64)
	managers.network:session():send_to_peers_synched('sync_body_damage_bullet', col_ray.body, self.mtga_smacker, normal_vec_yaw, normal_vec_pitch, col_ray.position, dir_vec_yaw, dir_vec_pitch, 16384)

	self._unit:damage():run_sequence_simple('int_seq_helmet_glass_02')
end

function IntimitateInteractionExt:mtga_finish_him(player)
	mvec3_set(tmp_vec, self._unit:movement():m_head_rot():y() * -1)
	local damage_info = {
		attacker_unit = player,
		damage = self._unit:character_damage()._health,
		col_ray = {
			body = self._unit:body('body'),
			unit = self._unit,
			ray = tmp_vec,
			normal = tmp_vec,
			position = self.mtga_smacker:position()
		}
	}
	self._unit:character_damage():damage_melee(damage_info)
	World:effect_manager():spawn({
		effect = ids_bullet_hit_blood,
		position = self.mtga_smacker:position(),
		normal = tmp_vec
	})
end

local mtga_original_intimitateinteractionext_interact = IntimitateInteractionExt.interact
function IntimitateInteractionExt:interact(player)
	if not self:can_interact(player) then
		return
	end

	if self.tweak_data == MTGA.interaction_name then
		if self._tweak_data.sound_event then
			player:sound():play(self._tweak_data.sound_event)
		end

		if Network:is_server() then
			self:mtga_start(player)
		else
			LuaNetworking:SendToPeer(1, MTGA.smack_network_id, self._unit:id())
		end

		return
	end

	mtga_original_intimitateinteractionext_interact(self, player)
end

function IntimitateInteractionExt:mtga_start(player)
	if self.mtga_in_progress then
		return
	end

	local smacker_unit = MTGA:link_smacker_to_bulldo(self._unit)
	if not smacker_unit then
		return
	end
	self.mtga_smacker = smacker_unit

	if smacker_unit:name() == MTGA.ids_drill then
		DelayedCalls:Add('DelayedModMTGA1_' .. tostring(self._unit:key()), 2, function()
			self:mtga_drill_step_1(player)
		end)
	else
		DelayedCalls:Add('DelayedModMTGA1_' .. tostring(self._unit:key()), 6, function()
			self:mtga_c4_step_1(player)
		end)
	end

	if alive(player) then
		self._unit:movement():set_attention({unit = player})
		managers.groupai:state():chk_say_enemy_chatter(self._unit, self._unit:position(), 'contact')
	end
end

function IntimitateInteractionExt:mtga_c4_step_1(player)
	if not alive(self._unit) or self._unit:id() == -1 then
		self:mtga_cleanup()
		return
	end

	self.mtga_smacker:base():_play_sound_and_effects()
	self:mtga_break_plate()
	self:mtga_break_glass()
	MTGA:unlink_smacker(self.mtga_smacker)
	self:mtga_finish_him(player)

	self:mtga_cleanup()
end

function IntimitateInteractionExt:mtga_drill_step_1(player)
	if not alive(self._unit) or self._unit:id() == -1 then
		self:mtga_cleanup()
		return
	end

	self:mtga_break_plate()

	DelayedCalls:Add('DelayedModMTGA2_' .. tostring(self._unit:key()), 2, function()
		self:mtga_drill_step_2(player)
	end)
end

function IntimitateInteractionExt:mtga_drill_step_2(player)
	if not alive(self._unit) or self._unit:id() == -1 then
		self:mtga_cleanup()
		return
	end

	self:mtga_break_glass()

	DelayedCalls:Add('DelayedModMTGA3_' .. tostring(self._unit:key()), 2, function()
		self:mtga_drill_step_3(player)
	end)
end

function IntimitateInteractionExt:mtga_drill_step_3(player)
	if not alive(self._unit) or self._unit:id() == -1 then
		self:mtga_cleanup()
		return
	end

	MTGA:unlink_smacker(self.mtga_smacker)
	self:mtga_finish_him(player)

	DelayedCalls:Add('DelayedModMTGA4_' .. tostring(self._unit:key()), 1, function()
		self:mtga_cleanup()
	end)
end

function IntimitateInteractionExt:mtga_cleanup()
	if alive(self.mtga_smacker) then
		World:delete_unit(self.mtga_smacker)
	end
	self.mtga_smacker = nil
	self.mtga_in_progress = nil
end
