local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

dofile(ModPath .. 'lua/_mtga.lua')

local mtga_original_interactiontweakdata_init = InteractionTweakData.init
function InteractionTweakData:init(...)
	mtga_original_interactiontweakdata_init(self, ...)

	self[MTGA.interaction_name] = {
		action_text_id = 'hud_action_placing_drill',
		axis = 'y',
		force_update_position = true,
		icon = 'equipment_drill',
		interact_distance = 250,
		interaction_obj = Idstring('Spine2'),
		sound_start = 'bar_secure_winch',
		sound_interupt = 'bar_secure_winch_cancel',
		sound_done = 'bar_secure_winch_finished',
		text_id = 'hud_int_equipment_drill',
		timer = 1.5,
		requires_upgrade = {
			category = 'player',
			upgrade = 'drill_melee_hit_restart_chance'
		}
	}
end
