local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

local mtga_original_unitnetworkhandler_lootlink = UnitNetworkHandler.loot_link
function UnitNetworkHandler:loot_link(loot_unit, parent_unit, sender)
	if not alive(loot_unit) or not alive(parent_unit) or not self._verify_gamestate(self._gamestate_filter.any_ingame) then
		return
	end

	if loot_unit:name() == MTGA.ids_drill or loot_unit:name() == MTGA.ids_c4 then
		if loot_unit:key() == parent_unit:key() then
			MTGA:unlink_smacker(loot_unit)
		else
			MTGA:link_smacker_to_bulldo(parent_unit, loot_unit)
		end
		return
	end

	mtga_original_unitnetworkhandler_lootlink(self, loot_unit, parent_unit, sender)
end
