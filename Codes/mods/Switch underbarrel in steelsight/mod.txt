{
	"blt_version" : 2,
	"name" : "Switch underbarrel in steelsight",
	"description" : "",
	"author" : "TdlQ",
	"image" : "tdlq.png",
	"color" : "0.52 1 0",
	"contact" : "",
	"version" : "6",
	"simple_update_url" : "http://pd2mods.z77.fr/update/SwitchUnderbarrelInSteelsight",
	"hooks" : [
		{
			"hook_id" : "lib/units/beings/player/states/playerstandard",
			"script_path" : "lua/playerstandard.lua"
		}
	]
}