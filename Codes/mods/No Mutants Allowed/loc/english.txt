{
	"nma_options_menu_title" : "No Mutants Allowed",
	"nma_options_menu_desc"  : "",

	"nma_options_mark_cheater_title" : "Mark cheater",
	"nma_options_mark_cheater_desc"  : "Apply game's red flag on players detected as cheaters.",

	"nma_options_mark_cheater_on_xth_reason_title" : "Wait xth reason to mark cheater",
	"nma_options_mark_cheater_on_xth_reason_desc"  : "Parameter ignored if option 'Mark cheater' is disabled.",

	"nma_options_log_all_anomalies_title" : "Log all anomalies",
	"nma_options_log_all_anomalies_desc"  : "If unchecked, it will announce only the first one.",

	"nma_options_max_ping_to_eval_elapsed_time_title" : "Max ping to eval elapsed time",
	"nma_options_max_ping_to_eval_elapsed_time_desc"  : "Expressed in milliseconds. Players having really high network latency.",

	"nma_options_disable_time_effects_title" : "Disable time effects",
	"nma_options_disable_time_effects_desc"  : "Time effects can be slow motion occuring when going down, they mess with synch while they're active.",

	"nma_options_keybind_report_title" : "Show player's report",
	"nma_options_keybind_report_desc"  : "Display information about the player you're looking at.",

	"nma_inspect_player_title" : "NoMA files",
	"nma_inspect_player_descr" : "See some of the data gathered by No Mutants Allowed."
}
