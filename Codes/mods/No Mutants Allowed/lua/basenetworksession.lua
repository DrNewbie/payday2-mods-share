local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

local nma_original_basenetworksession_onpeerremoved = BaseNetworkSession._on_peer_removed
function BaseNetworkSession:_on_peer_removed(peer, peer_id, reason)
	nma_original_basenetworksession_onpeerremoved(self, peer, peer_id, reason)
	NoMA:uninitialize_player_profile(peer_id)
end

local nma_original_basenetworksession_spawnplayers = BaseNetworkSession.spawn_players
function BaseNetworkSession:spawn_players(...)
	NoMA.spawn_t = TimerManager:game():time()
	return nma_original_basenetworksession_spawnplayers(self, ...)
end
