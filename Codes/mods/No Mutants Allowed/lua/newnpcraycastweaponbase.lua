local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

local _account_attack = function() end

DelayedCalls:Add('DelayedModNMA_sessionpeers', 1, function()
	local _peers = managers.network:session() and managers.network:session():peers() or {}
	local _max_players = tweak_data.max_players
	local _hit_accounting = NoMA.hit_accounting
	_account_attack = function(target_unit)
		if target_unit then
			local t_key = target_unit:key()
			for peer_id = _max_players, 1, -1 do
				local peer = _peers[peer_id]
				if peer and peer._unit and t_key == peer._unit:key() then
					local hacc = _hit_accounting[peer_id]
					if hacc then
						hacc.attacked_nr = hacc.attacked_nr + 1
					end
				end
			end
		end
	end
end)

for _, obj in pairs({NPCRaycastWeaponBase, NewNPCRaycastWeaponBase}) do
	for _, func in pairs({'trigger_held', 'singleshot'}) do
		local nma_original_nrwb = obj[func]
		obj[func] = function(...)
			local fired = nma_original_nrwb(...)
			if fired then
				_account_attack(select(9, ...))
			end
			return fired
		end
	end
end
