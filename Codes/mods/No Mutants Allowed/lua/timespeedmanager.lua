local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

local nma_original_timespeedmanager_playeffect = TimeSpeedManager.play_effect
function TimeSpeedManager:play_effect(...)
	if NoMA.settings.disable_time_effects then
		return
	end
	NoMA.timespeedchange_t = self._game_timer:time()
	return nma_original_timespeedmanager_playeffect(self, ...)
end

local nma_original_timespeedmanager_stopeffect = TimeSpeedManager.stop_effect
function TimeSpeedManager:stop_effect(...)
	if not NoMA.settings.disable_time_effects then
		NoMA.timespeedchange_t = self._game_timer:time()
	end
	return nma_original_timespeedmanager_stopeffect(self, ...)
end

local nma_original_timespeedmanager_oneffectexpired = TimeSpeedManager._on_effect_expired
function TimeSpeedManager:_on_effect_expired(...)
	NoMA.timespeedchange_t = self._game_timer:time()
	return nma_original_timespeedmanager_oneffectexpired(self, ...)
end
