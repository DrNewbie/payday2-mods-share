local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

local tmp = {}
local nma_original_connectionnetworkhandler_syncoutfit = ConnectionNetworkHandler.sync_outfit
function ConnectionNetworkHandler:sync_outfit(outfit_string, outfit_version, outfit_signature, sender)
	nma_original_connectionnetworkhandler_syncoutfit(self, outfit_string, outfit_version, outfit_signature, sender)

	if not game_state_machine:verify_game_state(GameStateFilters.lobby) then
		local peer = self._verify_sender(sender)
		if peer and NoMA then
			local profile = NoMA:get_player_profile(peer)
			if not profile then
				NoMA:check_outfit(peer)
			elseif profile.outfit_string ~= outfit_string then
				profile.is_changing = true
				local peer_id = peer:id()
				tmp[peer_id] = outfit_string
				DelayedCalls:Add('DelayedModNoMA_connectionnetworkhandler_syncoutfit_' .. peer_id, 0.5, function()
					if tmp[peer_id] == outfit_string then
						profile.is_changing = nil
						if managers.network:session() and managers.network:session():peer(peer_id) == peer then
							NoMA:check_outfit(peer)
						end
					end
				end)
			end
		end
	end
end

local nma_original_connectionnetworkhandler_preplanningreserved = ConnectionNetworkHandler.preplanning_reserved
function ConnectionNetworkHandler:preplanning_reserved(type, id, peer_id, state, sender)
	if not self._verify_sender(sender) then
		return
	end

	nma_original_connectionnetworkhandler_preplanningreserved(self, type, id, peer_id, state, sender)

	local asset = tweak_data.preplanning.types[type]
	if asset and asset.upgrade_lock then
		NoMA:check_upgrade(managers.network:session():peer(peer_id), asset.upgrade_lock.category .. '_' .. asset.upgrade_lock.upgrade)
	end
end

local nma_original_connectionnetworkhandler_reservepreplanning = ConnectionNetworkHandler.reserve_preplanning
function ConnectionNetworkHandler:reserve_preplanning(type, id, state, sender)
	local peer = self._verify_sender(sender)
	if not peer then
		return
	end

	nma_original_connectionnetworkhandler_reservepreplanning(self, type, id, state, sender)

	local asset = tweak_data.preplanning.types[type]
	if asset and asset.upgrade_lock then
		NoMA:check_upgrade(peer, asset.upgrade_lock.category .. '_' .. asset.upgrade_lock.upgrade)
	end
end
