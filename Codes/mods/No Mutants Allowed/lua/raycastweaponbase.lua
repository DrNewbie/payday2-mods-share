local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

function RaycastWeaponBase:ammo_info()
	local abase = self:ammo_base()
	return abase:get_ammo_max_per_clip(), abase:get_ammo_remaining_in_clip(), abase:get_ammo_total(), abase:get_ammo_max(), self:selection_index()
end

DelayedCalls:Add('DelayedModNMA_setammoamount', 0, function()
	if HUDManager then
		local nma_original_hudmanager_setammoamount = HUDManager.set_ammo_amount
		function HUDManager:set_ammo_amount(selection_index, max_clip, current_clip, current_left, max, real_selection_index)
			nma_original_hudmanager_setammoamount(self, real_selection_index or selection_index, max_clip, current_clip, current_left, max)
		end
	end
end)
