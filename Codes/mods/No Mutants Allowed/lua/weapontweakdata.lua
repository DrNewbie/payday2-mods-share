for weapon_id, weapon in pairs(tweak_data.weapon) do
	if weapon.use_data and not weapon.name_id then
		local parent_weapon = tweak_data.weapon[weapon_id:gsub('_crew$', ''):gsub('_npc$', '')]
		local parent_selection_index = parent_weapon and parent_weapon.use_data and parent_weapon.use_data.selection_index
		if parent_selection_index and parent_selection_index ~= weapon.use_data.selection_index then
			log('[NoMA] fixed selection index of ' .. weapon_id)
			weapon.use_data.selection_index = parent_selection_index
		end
	end
end
