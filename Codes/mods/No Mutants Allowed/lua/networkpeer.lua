local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

local nma_original_networkpeer_onlost = NetworkPeer.on_lost
function NetworkPeer:on_lost()
	NoMA:uninitialize_player_profile(self:id())
	nma_original_networkpeer_onlost(self)
end

local nma_original_networkpeer_verifygrenade = NetworkPeer.verify_grenade
function NetworkPeer:verify_grenade(value)
	if value == -1 then
		if self ~= managers.network:session():local_peer() then
			local profile = NoMA:get_player_profile(self)
			if profile.throwable.reusable then
				-- qued
			elseif profile.previous_interaction_id == 'grenade_crate' then
				-- qued
			elseif managers.player:upgrade_value('team', 'crew_throwable_regen', 0) ~= 0 then
				-- qued
			else
				NoMA:check_upgrade(self, 'player_regain_throwable_from_ammo_1')
			end
		end
	end

	return nma_original_networkpeer_verifygrenade(self, value)
end
