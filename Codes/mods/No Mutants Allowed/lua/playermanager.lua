local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

local nma_original_playermanager_addsyncedteamupgrade = PlayerManager.add_synced_team_upgrade
function PlayerManager:add_synced_team_upgrade(peer_id, category, upgrade, level)
	nma_original_playermanager_addsyncedteamupgrade(self, peer_id, category, upgrade, level)
	NoMA:check_upgrade(managers.network:session():peer(peer_id), nil, category, upgrade, level)
end
