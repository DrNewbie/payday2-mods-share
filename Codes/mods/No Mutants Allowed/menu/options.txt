{
	"menu_id" : "nma_options_menu",
	"parent_menu_id" : "blt_options",
	"title" : "nma_options_menu_title",
	"description" : "nma_options_menu_desc",
	"back_callback" : "NoMutantsAllowed_MenuSave",
	"items" : [
		{
			"type" : "toggle",
			"id" : "log_all_anomalies",
			"title" : "nma_options_log_all_anomalies_title",
			"description" : "nma_options_log_all_anomalies_desc",
			"callback" : "NoMutantsAllowed_MenuCheckboxClbk",
			"value" : "log_all_anomalies"
		},
		{
			"type" : "toggle",
			"id" : "mark_cheater",
			"title" : "nma_options_mark_cheater_title",
			"description" : "nma_options_mark_cheater_desc",
			"callback" : "NoMutantsAllowed_MenuCheckboxClbk",
			"value" : "mark_cheater"
		},
		{
			"type" : "slider",
			"id" : "mark_cheater_on_xth_reason",
			"title" : "nma_options_mark_cheater_on_xth_reason_title",
			"description" : "nma_options_mark_cheater_on_xth_reason_desc",
			"callback" : "NoMutantsAllowed_MenuSliderClbk",
			"value" : "mark_cheater_on_xth_reason",
			"default_value" : 1,
			"min" : 1,
			"max" : 10,
			"step" : 1
		},
		{
			"type" : "divider",
			"size" : 16
		},
		{
			"type" : "keybind",
			"id" : "nma_keybind_report",
			"title" : "nma_options_keybind_report_title",
			"description" : "nma_options_keybind_report_desc",
			"keybind_id" : "noma_report",
			"run_in_game" : true,
			"func" : "NoMAKeybindShowPlayerReport"
		},
		{
			"type" : "divider",
			"size" : 16
		},
		{
			"type" : "slider",
			"id" : "max_ping_to_eval_elapsed_time",
			"title" : "nma_options_max_ping_to_eval_elapsed_time_title",
			"description" : "nma_options_max_ping_to_eval_elapsed_time_desc",
			"callback" : "NoMutantsAllowed_MenuSliderClbk",
			"value" : "max_ping_to_eval_elapsed_time",
			"default_value" : 300,
			"min" : 0,
			"max" : 1000,
			"step" : 10
		},
		{
			"type" : "toggle",
			"id" : "disable_time_effects",
			"title" : "nma_options_disable_time_effects_title",
			"description" : "nma_options_disable_time_effects_desc",
			"callback" : "NoMutantsAllowed_MenuCheckboxClbk",
			"value" : "disable_time_effects"
		}
	]
}
