{
	"mrk_options_menu_title" : "Marking",
	"mrk_options_menu_desc"  : "Este mod cambia varias cosas en el sistema de marcado.",

	"mrk_options_mark_aim_title" : "Marcar lo que se apunta",
	"mrk_options_mark_aim_desc" : "Ordena los objetivos solo por ángulo (ignora la distancia).\nSe requiere reiniciar el atraco para que se tenga en cuenta este cambio.",

	"mrk_options_ignore_disabled_cams_title" : "Evitar marcar cámaras deshabilitadas",
	"mrk_options_ignore_disabled_cams_desc" : "Una vez que se elimina al operador de la cámara, el sistema de marcado ignora las cámaras.",

	"mrk_options_disable_sentry_switch_mode_title" : "Deshabilitar el modo de disparo de la torreta",
	"mrk_options_disable_sentry_switch_mode_desc" : "Deshabilita la posibilidad de interacción para cambiar el modo de disparo de la Torreta Portátil.",

	"mrk_options_prioritize_intimidated_title" : "Dar más prioridad a los policías intimidados",
	"mrk_options_prioritize_intimidated_desc" : "Los policías que aún no han sido capturados por completo reciben una bonificación en la prioridad al gritar.\nEsta opción no tiene efecto cuando \"Marcar lo que se apunta\" está habilitado."
}