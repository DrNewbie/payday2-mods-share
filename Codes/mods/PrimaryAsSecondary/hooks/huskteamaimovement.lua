function HuskTeamAIMovement:add_weapons()
	local weapon = self._ext_base:default_weapon_name("primary")
	local _ = weapon and self._unit:inventory():add_unit_by_factory_name_selection_index(weapon, false, false, nil, "", 2)
	local sec_weap_name = self._ext_base:default_weapon_name("secondary")
	local _ = sec_weap_name and sec_weap_name ~= weapon and self._unit:inventory():add_unit_by_name_selection_index(sec_weap_name, false, 1)
end