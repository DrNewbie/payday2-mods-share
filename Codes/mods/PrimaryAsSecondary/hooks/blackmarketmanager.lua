BlackMarketManager.old_get_weapon_category = BlackMarketManager.old_get_weapon_category or BlackMarketManager.get_weapon_category
function BlackMarketManager:get_weapon_category( category )
	if ( SecondaryAsPrimaryGlobalStatus and category == "primaries" ) then
		local t = {}

		for weapon_name, weapon_data in pairs(self._global.weapons) do
			if weapon_name ~= "saw_secondary" and ( weapon_data.selection_index == 1 or weapon_data.selection_index == 2 ) then
				table.insert(t, weapon_data)

				t[#t].weapon_id = weapon_name
			end
		end

		return t
	elseif ( PrimaryAsSecondaryGlobalStatus and category == "secondaries" ) then
		local t = {}

		for weapon_name, weapon_data in pairs(self._global.weapons) do
			if weapon_name ~= "saw" and ( weapon_data.selection_index == 1 or weapon_data.selection_index == 2 ) then
				table.insert(t, weapon_data)

				t[#t].weapon_id = weapon_name
			end
		end

		return t
	else
		return self:old_get_weapon_category(category)
	end
end