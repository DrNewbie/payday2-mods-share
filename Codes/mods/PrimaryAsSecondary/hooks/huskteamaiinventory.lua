function HuskTeamAIInventory:add_unit_by_factory_name_selection_index(factory_name, equip, instant, blueprint_string, cosmetics_string, selection_index)
	local factory_weapon = tweak_data.weapon.factory[factory_name]
	local ids_unit_name = Idstring(factory_weapon.unit)
	local blueprint = nil

	if blueprint_string and blueprint_string ~= "" then
		blueprint = managers.weapon_factory:unpack_blueprint_from_string(factory_name, blueprint_string)
	else
		blueprint = managers.weapon_factory:get_default_blueprint_by_factory_id(factory_name)
	end

	local cosmetics = managers.blackmarket:cosmetics_from_outfit_string(cosmetics_string)

	self:add_unit_by_factory_blueprint_selection_index(factory_name, equip, instant, blueprint, cosmetics, selection_index)
end

function HuskTeamAIInventory:add_unit_by_factory_blueprint_selection_index(factory_name, equip, instant, blueprint, cosmetics, selection_index)
	local factory_weapon = tweak_data.weapon.factory[factory_name]
	local ids_unit_name = Idstring(factory_weapon.unit)

	managers.dyn_resource:load(Idstring("unit"), ids_unit_name, managers.dyn_resource.DYN_RESOURCES_PACKAGE, nil)

	local new_unit = World:spawn_unit(Idstring(factory_weapon.unit), Vector3(), Rotation())

	new_unit:base():set_factory_data(factory_name)
	new_unit:base():set_cosmetics_data(cosmetics)
	new_unit:base():assemble_from_blueprint(factory_name, blueprint)
	new_unit:base():check_npc()

	local ignore_units = {
		self._unit,
		new_unit
	}

	if self._ignore_units then
		for idx, ig_unit in pairs(self._ignore_units) do
			table.insert(ignore_units, ig_unit)
		end
	end

	local setup_data = {
		user_unit = self._unit,
		ignore_units = ignore_units,
		expend_ammo = false,
		autoaim = false,
		alert_AI = false,
		user_sound_variant = "1"
	}

	new_unit:base():setup(setup_data)

	if new_unit:base().AKIMBO then
		local first, second = self:_align_place(equip, new_unit, "left_hand")

		new_unit:base():create_second_gun(nil, second and second.obj3d_name or first.obj3d_name)
	end

	self:add_unit_selection_index(new_unit, equip, instant, selection_index)
end

function HuskTeamAIInventory:add_unit_by_name_selection_index(new_unit_name, equip, selection_index)
	local new_unit = World:spawn_unit(new_unit_name, Vector3(), Rotation())
	local ignore_units = {
		self._unit,
		new_unit
	}

	if self._ignore_units then
		for idx, ig_unit in pairs(self._ignore_units) do
			table.insert(ignore_units, ig_unit)
		end
	end

	local setup_data = {
		user_unit = self._unit,
		ignore_units = ignore_units,
		expend_ammo = false,
		hit_slotmask = managers.slot:get_mask("bullet_impact_targets_no_AI"),
		hit_player = false,
		user_sound_variant = tweak_data.character[self._unit:base()._tweak_table].weapon_voice
	}

	new_unit:base():setup(setup_data)
	self:add_unit_selection_index(new_unit, equip, false, selection_index)
end

function HuskTeamAIInventory:add_unit_selection_index(new_unit, equip, selection_index)
	CopInventory.super.add_unit_selection_index(self, new_unit, equip, false, selection_index)

	new_unit:set_enabled(true)
	new_unit:set_visible(true)

	self:_ensure_weapon_visibility(new_unit)
end