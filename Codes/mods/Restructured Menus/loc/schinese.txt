{
	"rm_options_menu_title" : "菜单重组",
	"rm_options_menu_desc"  : "",

	"rm_options_main_hide_crimenet_online_title" : "隐藏在线游戏",
	"rm_options_main_hide_crimenet_online_desc"  : "隐藏主菜单中的在线游戏按钮。",
	"rm_options_main_hide_crimenet_offline_title" : "隐藏离线游戏",
	"rm_options_main_hide_crimenet_offline_desc"  : "隐藏主菜单中的离线游戏按钮。",
	"rm_options_main_add_crimenet_broker_title" : "添加合约联系人",
	"rm_options_main_add_crimenet_broker_desc"  : "在主菜单中显示合约联系人。",
	"rm_options_main_add_side_jobs_title" : "添加额外挑战",
	"rm_options_main_add_side_jobs_desc"  : "在主菜单中显示额外挑战。",
	"rm_options_hide_finished_story_missions_title" : "生涯模式通关后隐藏",
	"rm_options_hide_finished_story_missions_desc"  : "主菜单与大厅均生效。",
	"rm_options_lobby_add_side_jobs_title" : "添加额外挑战",
	"rm_options_lobby_add_side_jobs_desc"  : "在联机大厅中显示额外挑战。",
	"rm_options_lobby_add_contract_broker_title" : "添加合约联系人",
	"rm_options_lobby_add_contract_broker_desc"  : "在联机大厅中显示合约联系人。",
	"rm_options_lobby_add_steam_inventory_title" : "添加 Steam 库存",
	"rm_options_lobby_add_steam_inventory_desc"  : "在联机大厅中显示 Steam 库存。"
}