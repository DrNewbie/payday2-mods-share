local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

_G.RestructuredMenus = _G.RestructuredMenus or {}
RestructuredMenus._path = ModPath
RestructuredMenus._data_path = SavePath .. 'restructured_menus.txt'
RestructuredMenus.settings = {
	main_hide_crimenet_online = false,
	main_hide_crimenet_offline = false,
	main_add_crimenet_broker = true,
	main_add_side_jobs = true,
	hide_finished_story_missions = true,
	lobby_add_side_jobs = true,
	lobby_add_contract_broker = true,
	lobby_add_steam_inventory = true,
}

function RestructuredMenus:load()
	local file = io.open(self._data_path, 'r')
	if file then
		for k, v in pairs(json.decode(file:read('*all')) or {}) do
			self.settings[k] = v
		end
		file:close()
	end
end

function RestructuredMenus:save()
	local file = io.open(self._data_path, 'w+')
	if file then
		file:write(json.encode(self.settings))
		file:close()
	end
end

function MenuHelper:GetMenuItem(parent_menu, child_menu_name)
	for i, item in pairs(parent_menu._items) do
		if item._parameters.name == child_menu_name then
			return i, item
		end
	end
end

function MenuHelper:AddVisibleCallback(parent_menu, child_menu_name, callback_name)
	local _, item = self:GetMenuItem(parent_menu, child_menu_name)
	if not item then
		return false
	end

	if type(item._visible_callback_name_list) == 'table' then
		if table.contains(item._visible_callback_name_list, callback_name) then
			return false
		end
	else
		item._visible_callback_name_list = {}
	end

	table.insert(item._visible_callback_name_list, callback_name)
	item._parameters.visible_callback = table.concat(item._visible_callback_name_list, ' ')
	item:set_callback_handler(item._callback_handler)

	return true
end

function MenuHelper:AddExistingMenuItem(parent_menu, item)
	if parent_menu and item then
		table.insert(parent_menu._items, item)
	end
end

function MenuHelper:RemoveMenuItem(parent_menu, child_menu_name)
	local index = self:GetMenuItem(parent_menu, child_menu_name)
	if index then
		return table.remove(parent_menu._items, index)
	end
end

function MenuHelper:HideMenuItem(parent_menu, child_menu_name)
	self:AddVisibleCallback(parent_menu, child_menu_name, 'rm_return_false')
end

function MenuHelper:MoveMenuItem(parent_menu, moved_menu_name, new_pos, direction)
	local item = self:RemoveMenuItem(parent_menu, moved_menu_name)
	if item then
		local final_pos
		if type(new_pos) == 'number' then
			final_pos = new_pos
		elseif type(new_pos) == 'string' then
			final_pos = self:GetMenuItem(parent_menu, new_pos)
			if not final_pos then
				return
			end
			if not direction or direction == 'after' then
				final_pos = final_pos + 1
			end
		end

		if final_pos then
			table.insert(parent_menu._items, final_pos, item)
		end
	end
end

function MenuHelper:SetIcon(parent_menu, child_menu_name, icon, callback_name)
	local _, item = self:GetMenuItem(parent_menu, child_menu_name)
	if item then
		item._parameters.icon = icon
		item._parameters.icon_visible_callback = { callback_name }
		item._icon_visible_callback_name_list  = { callback_name }
		item:set_callback_handler(item._callback_handler)
	end
end

Hooks:Add('LocalizationManagerPostInit', 'LocalizationManagerPostInit_RestructuredMenus', function(loc)
	local language_filename

	for _, filename in pairs(file.GetFiles(RestructuredMenus._path .. 'loc/')) do
		local str = filename:match('^(.*).txt$')
		if str and Idstring(str) and Idstring(str):key() == SystemInfo:language():key() then
			language_filename = filename
			break
		end
	end

	if language_filename then
		loc:load_localization_file(RestructuredMenus._path .. 'loc/' .. language_filename)
	end
	loc:load_localization_file(RestructuredMenus._path .. 'loc/english.txt', false)
end)

Hooks:Add('MenuManagerInitialize', 'MenuManagerInitialize_RestructuredMenus', function(menu_manager)
	MenuHelper:LoadFromJsonFile(RestructuredMenus._path .. 'menu/options.txt', RestructuredMenus, RestructuredMenus.settings)

	function MenuCallbackHandler:RestructuredMenusToggleHub(item)
		RestructuredMenus.settings[item:name()] = item:value() == 'on'
	end

	function MenuCallbackHandler:RestructuredMenusSave()
		RestructuredMenus:save()
	end

	function MenuCallbackHandler:rm_return_false()
		return false
	end

	function MenuCallbackHandler:rm_story_finished()
		local nm = managers.story and managers.story:_find_next_mission(true)
		return not (nm and nm.id == 'sm_end')
	end
end)

Hooks:Add('MenuManagerBuildCustomMenus', 'MenuManagerBuildCustomMenus_RestructuredMenus', function(menu_manager, nodes)
	local node = nodes.side_jobs
	if node then
		node:parameters().sync_state = 'payday'
	end

	node = nodes.steam_inventory
	if node then
		node:parameters().sync_state = 'payday'
	end

	node = nodes.main
	if node then
		if RestructuredMenus.settings.main_hide_crimenet_online then
			MenuHelper:HideMenuItem(node, 'crimenet')
		end
		if RestructuredMenus.settings.main_hide_crimenet_offline then
			MenuHelper:HideMenuItem(node, 'crimenet_offline')
		end
		if RestructuredMenus.settings.main_add_crimenet_broker then
			MenuHelper:AddMenuItem(node, 'contract_broker', 'menu_contract_broker', '', 'crimenet_offline', 'after')
		end
		if RestructuredMenus.settings.main_add_side_jobs then
			MenuHelper:AddMenuItem(node, 'side_jobs', 'menu_cn_challenge', 'menu_cn_challenge_desc', 'steam_inventory', 'before')
			MenuHelper:SetIcon(node, 'side_jobs', 'guis/textures/pd2/icon_reward', 'show_side_job_menu_icon')
		end
		if RestructuredMenus.settings.hide_finished_story_missions then
			MenuHelper:AddVisibleCallback(node, 'story_missions', 'rm_story_finished')
		end
	end

	node = nodes.lobby
	if node then
		if RestructuredMenus.settings.lobby_add_side_jobs then
			MenuHelper:AddMenuItem(node, 'side_jobs', 'menu_cn_challenge', 'menu_cn_challenge_desc', 'edit_game_settings', 'before')
			MenuHelper:SetIcon(node, 'side_jobs', 'guis/textures/pd2/icon_reward', 'show_side_job_menu_icon')
		end
		if RestructuredMenus.settings.lobby_add_contract_broker then
			MenuHelper:AddMenuItem(node, 'contract_broker', 'menu_contract_broker', '', 'crimenet_nj', 'before')
			MenuHelper:AddVisibleCallback(node, 'contract_broker', 'is_server')
		end
		if RestructuredMenus.settings.lobby_add_steam_inventory then
			MenuHelper:AddMenuItem(node, 'steam_inventory', 'menu_steam_inventory', 'menu_steam_inventory_help', 'inventory', 'after')
		end
		if RestructuredMenus.settings.hide_finished_story_missions then
			MenuHelper:AddVisibleCallback(node, 'story_missions', 'rm_story_finished')
		end
	end

	node = nodes.crime_spree_lobby
	if node then
		MenuHelper:AddMenuItem(node, 'side_jobs', 'menu_cn_challenge', 'menu_cn_challenge_desc', 'inventory', 'after')
		MenuHelper:SetIcon(node, 'side_jobs', 'guis/textures/pd2/icon_reward', 'show_side_job_menu_icon')
	end
end)

local rm_original_menuoptioninitiator_modifyadvvideo = MenuOptionInitiator.modify_adv_video
function MenuOptionInitiator:modify_adv_video(...)
	local node = rm_original_menuoptioninitiator_modifyadvvideo(self, ...)

	for _, option in pairs(node:item('choose_corpse_limit')._options) do
		option._parameters.localize = false
		option._parameters.text_id = option._parameters.value
	end

	return node
end

local rm_original_menucallbackhandler_startthegame = MenuCallbackHandler.start_the_game
function MenuCallbackHandler:start_the_game()
	if not managers.network:session() then
		managers.network:host_game()
	end

	rm_original_menucallbackhandler_startthegame(self)
end

RestructuredMenus:load()
