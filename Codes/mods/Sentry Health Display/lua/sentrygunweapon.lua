local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

local shd_original_sentrygunweapon_changeammo = SentryGunWeapon.change_ammo
function SentryGunWeapon:change_ammo(...)
	shd_original_sentrygunweapon_changeammo(self, ...)
	self:shd_update_label()
end

local shd_original_sentrygunweapon_fire = SentryGunWeapon.fire
function SentryGunWeapon:fire(...)
	local result = self._muzzle_effect_table and shd_original_sentrygunweapon_fire(self, ...)
	self:shd_update_label()
	return result
end

function SentryGunWeapon:shd_get_label_text()
	local ammo_ratio = Network:is_server() and self:ammo_ratio() or self:get_virtual_ammo_ratio()
	local str = managers.localization:text(self._unit:base():interaction_text_id(), {AMMO_LEFT = math.round(ammo_ratio * 100)})
	local owner = self._owner or managers.player:player_unit()

	local masks = {
		'%d+%%',
		'\n(.*)\.'
	}
	local start_i = alive(owner) and mvector3.distance(owner:position(), self._unit:position()) > 700 and 1 or 2
	for i = start_i, 1, -1 do
		local ammo = str:match(masks[i])
		if ammo then
			return ammo
		end
	end
end

function SentryGunWeapon:shd_update_label()
	if not self.shd_display_info then
		return
	end

	local name_label = managers.hud:_get_name_label(self._unit:unit_data().name_label_id)
	if name_label then
		local text = name_label.panel:child('text')
		if text then
			text:set_text(self:shd_get_label_text())
			managers.hud:align_teammate_name_label(name_label.panel, name_label.interact)
		end
	end
end
