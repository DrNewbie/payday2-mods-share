local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

local shd_original_sentryguncontour_setcontour = SentryGunContour._set_contour
function SentryGunContour:_set_contour(...)
	shd_original_sentryguncontour_setcontour(self, ...)

	DelayedCalls:Add('SentryGunContour' .. tostring(self._unit:key()), 0, function()
		if self._current_contour_id then
			local contour_ext = self._unit:contour()
			local contour = contour_ext and contour_ext._contour_list and contour_ext._contour_list[1]
			if contour then
				local name_label = managers.hud:_get_name_label(self._unit:unit_data().name_label_id)
				if name_label then
					local text = name_label.panel:child('text')
					if text then
						local c = ContourExt._types[contour.type].color or contour.color
						if c then
							text:set_color(Color(c.x, c.y, c.z))
						end
					end
				end
			end
		end
	end)
end
