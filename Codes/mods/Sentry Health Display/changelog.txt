Revision 13:
- fixed label not removed when a sentry is destroyed

Revision 12:
- fixed color of text label not correctly initialized

Revision 11:
- improved mod compatibility

Revision 10:
- added safer check of owner's unit

Revision 9:
- updated to U199.5

Revision 8:
- fixed a crash when deploying a sentry as a client

Revision 7:
- display full text only when close to sentry

Revision 6:
- changed update system to Simple Mod Updater

Revision 5:
- added a sanity check to prevent a crash occuring when a sentry fires without being properly setup

Revision 4:
- fixed health bug for clients
- added % of ammo left

Revision 3:
- updated to U162

Revision 2:
- fixed label not removed when playing as a client

Revision 1:
- initial release