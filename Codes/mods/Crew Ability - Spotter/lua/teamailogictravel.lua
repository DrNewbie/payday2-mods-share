local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

function TeamAILogicTravel.cas_upd_enemy_detection(data)
	data.t = TimerManager:game():time()
	CopLogicBase._upd_attention_obj_detection(data, nil, AIAttentionObject.REACT_SURPRISED)
	local my_data = data.internal_data
	TeamAILogicIdle._upd_sneak_spotting(data, my_data)
	CopLogicBase.queue_task(my_data, my_data.detection_task_key, TeamAILogicTravel.cas_upd_enemy_detection, data, data.t + 1)
end
