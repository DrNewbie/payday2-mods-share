local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

_G.CrewAbilitySpotter = _G.CrewAbilitySpotter or {}
CrewAbilitySpotter._path = ModPath
CrewAbilitySpotter.filter_ability = false
CrewAbilitySpotter.ability_name = 'crew_spotter'

function CrewAbilitySpotter:current_profile_has_ability()
	local profile = managers.multi_profile and managers.multi_profile:current_profile()
	if profile then
		for _, loadout in ipairs(profile.henchmen_loadout or {}) do
			if loadout.ability == self.ability_name then
				return true
			end
		end
	end
end

function CrewAbilitySpotter:peer_has_cas(peer)
	for _, mod in pairs(peer:synced_mods()) do
		if mod.name == 'Crew Ability: Spotter' then
			return true
		end
	end
end

function CrewAbilitySpotter:setup()
	local setting = clone(tweak_data.attention.settings.civ_enemy_corpse_sneak)
	setting.id = 'camera_spotter'
	setting.filter = managers.navigation:convert_access_filter_to_number({'teamAI1'})
	setting.reaction = AIAttentionObject.REACT_IDLE
	setting.verification_interval = 1

	for _, camera in ipairs(SecurityCamera.cameras) do
		if alive(camera) and camera:attention() then
			camera:attention():set_attention(setting)
		end
	end

	local cas_original_hostnetworksession_sendtopeerssynched = HostNetworkSession.send_to_peers_synched
	function HostNetworkSession:send_to_peers_synched(typ, ...)
		if typ == 'set_unit' then
			local params = {...}
			local loadout = params[3]
			if loadout:find(CrewAbilitySpotter.ability_name) then
				local patched_loadout = loadout:gsub(CrewAbilitySpotter.ability_name, 'nil')
				for peer_id, peer in pairs(self._peers) do
					params[3] = CrewAbilitySpotter:peer_has_cas(peer) and loadout or patched_loadout
					peer:send_queued_sync(typ, unpack(params))
				end
				return
			end
		end

		cas_original_hostnetworksession_sendtopeerssynched(self, typ, ...)
	end

	TeamAILogicIdle._upd_sneak_spotting = TeamAILogicIdle.cas_upd_sneak_spotting

	local cas_original_teamailogictravel_enter = TeamAILogicTravel.enter
	function TeamAILogicTravel.enter(data, ...)
		cas_original_teamailogictravel_enter(data, ...)

		if data.unit:movement():cool() then
			local my_data = data.internal_data
			my_data.detection_task_key = 'TeamAILogicTravel.cas_upd_enemy_detection' .. tostring(data.key)
			CopLogicBase.queue_task(my_data, my_data.detection_task_key, TeamAILogicTravel.cas_upd_enemy_detection, data, data.t)
		end
	end

	function TeamAIMovement:set_attention(attention)
		TeamAIMovement.super.set_attention(self, attention)

		if not managers.groupai:state():whisper_mode() then
			TeamAIMovement.set_attention = nil
		elseif self._attention and self._attention.unit then
			self._ext_network:send('cop_set_attention_pos', self._attention.unit:position())
		end
	end

	CrewAbilitySpotter.setup = function() end
end

Hooks:Add('LocalizationManagerPostInit', 'LocalizationManagerPostInit_CrewAbilitySpotter', function(loc)
	for _, filename in pairs(file.GetFiles(CrewAbilitySpotter._path .. 'loc/')) do
		local str = filename:match('^(.*).txt$')
		if str and Idstring(str) and Idstring(str):key() == SystemInfo:language():key() then
			loc:load_localization_file(CrewAbilitySpotter._path .. 'loc/' .. filename)
			break
		end
	end
	loc:load_localization_file(CrewAbilitySpotter._path .. 'loc/english.txt', false)
end)

local cas_original_crewmanagementgui_populatecustom = CrewManagementGui.populate_custom
function CrewManagementGui:populate_custom(category, henchman_index, tweak, list, ...)
	if category == 'ability' and not table.contains(list, CrewAbilitySpotter.ability_name) then
		table.insert(list, CrewAbilitySpotter.ability_name)
	end
	return cas_original_crewmanagementgui_populatecustom(self, category, henchman_index, tweak, list, ...)
end

local texture = 'guis/dlcs/mom/textures/pd2/ai_abilities_cas'

DB:create_entry(
	Idstring('texture'),
	Idstring(texture),
	ModPath .. 'assets/spotter.dds'
)

tweak_data.hud_icons[CrewAbilitySpotter.ability_name] = {
	texture_rect = { 0, 0, 128, 128 },
	texture = texture
}

tweak_data.upgrades.crew_ability_definitions[CrewAbilitySpotter.ability_name] = {
	icon = CrewAbilitySpotter.ability_name,
	name_id = 'menu_crew_spotter'
}

tweak_data.upgrades.values.team[CrewAbilitySpotter.ability_name] = { true }
