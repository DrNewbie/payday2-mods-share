local key = ModPath .. '	' .. RequiredScript
if _G[key] then return else _G[key] = true end

if Network:is_client() then
	return
end

local cas_original_teamaibase_save = TeamAIBase.save
function TeamAIBase:save(data)
	local original_ability = self._loadout.ability
	if original_ability == CrewAbilitySpotter.ability_name and CrewAbilitySpotter.filter_ability then
		self._loadout.ability = nil
	end

	cas_original_teamaibase_save(self, data)

	self._loadout.ability = original_ability
end

local cas_original_teamaibase_setloadout = TeamAIBase.set_loadout
function TeamAIBase:set_loadout(loadout)
	cas_original_teamaibase_setloadout(self, loadout)

	if loadout.ability == CrewAbilitySpotter.ability_name then
		CrewAbilitySpotter:setup()
	end
end
