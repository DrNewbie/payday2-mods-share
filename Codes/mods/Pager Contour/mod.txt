{
	"blt_version" : 2,
	"name" : "Pager Contour",
	"description" : "Sets a blue contour to answered pagers.",
	"author" : "TdlQ",
	"image" : "tdlq.dds",
	"color" : "0.52 1 0",
	"contact" : "",
	"version" : "7",
	"simple_update_url" : "http://pd2mods.z77.fr/update/PagerContour.zip",
	"hooks" : [
		{
			"hook_id" : "lib/managers/group_ai_states/groupaistatebase",
			"script_path" : "lua/groupaistatebase.lua"
		},
		{
			"hook_id" : "lib/units/contourext",
			"script_path" : "lua/contourext.lua"
		},
		{
			"hook_id" : "lib/units/interactions/interactionext",
			"script_path" : "lua/interactionext.lua"
		}
	]
}
