Revision 29:
- added "Use perk device in vehicle", http://pd2mods.z77.fr/use_perk_device_in_vehicle.html

Revision 28:
- added "Crew Ability: Pockets", http://pd2mods.z77.fr/crew_ability_pockets.html

Revision 27:
- added "No Laser When Aiming Down Sight"

Revision 26:
- added "Refresh Rate Checker", http://pd2mods.z77.fr/refresh_rate_checker.html

Revision 25:
- added "Celer", http://pd2mods.z77.fr/celer.html

Revision 24:
- added "No Armor Color Grading", http://pd2mods.z77.fr/no_armor_color_grading.html

Revision 23:
- added "Anticrash", http://pd2mods.z77.fr/anticrash.html

Revision 22:
- added "Crew Ability: Spotter", http://pd2mods.z77.fr/crew_ability_spotter.html

Revision 21:
- added "Fixed Mutators", http://pd2mods.z77.fr/fixed_mutators.html

Revision 20:
- removed "Sentry Auto AP" (added in base game by U199.5)

Revision 19:
- added "Crewfiles", http://pd2mods.z77.fr/crewfiles.html

Revision 18:
- readded "Switch underbarrel in steelsight" (lost in r16 migration), http://pd2mods.z77.fr/switch_underbarrel_in_steelsight.html

Revision 17:
- fixed dependency identifiers

Revision 16:
- changed update system to Simple Mod Updater

Revision 15:
- added "Sentry Auto AP"
- removed "Clear Texture Cache"

Revision 14:
- removed "Live Long and Prosper" (now included in Lobby Settings)

Revision 13:
- added "Saw Helper", http://pd2mods.z77.fr/saw_helper.html

Revision 12:
- added "Crew Ability: Pied Piper", http://pd2mods.z77.fr/crew_ability_pied_piper.html

Revision 11:
- added "Rename Inventory Pages", http://pd2mods.z77.fr/rename_inventory_pages.html

Revision 10:
- added "Yet Another Flashlight", http://pd2mods.z77.fr/yet_another_flashlight.html

Revision 9:
- added "No Duplicated Bullets", http://pd2mods.z77.fr/no_duplicated_bullets.html

Revision 8:
- added "Search Inventory", http://pd2mods.z77.fr/search_inventory.html

Revision 7:
- added "Less Inaccurate Weapon Laser", http://pd2mods.z77.fr/less_inaccurate_weapon_laser.html

Revision 6:
- removed "No V Allowed" as it's merged with Lobby Settings

Revision 5:
- added "Alphasort Mods", http://pd2mods.z77.fr/alphasort_mods.html

Revision 4:
- added "Clear Texture Cache", http://pd2mods.z77.fr/clear_texture_cache.html

Revision 3:
- added "Fix Crime Spree Concealment Modifier", http://pd2mods.z77.fr/fix_crime_spree_concealment_modifier.html

Revision 2:
- added "Marking", http://pd2mods.z77.fr/marking.html

Revision 1:
- initial release