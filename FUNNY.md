## 1. 「你叫我去覆盖？ 不就等于删了游戏文件？」

### 在这资讯发达的时代，手机人手一支，各种各样的科技都融入平常的生活

@光明中的一逗比

![ScreenShot](/Preview/Q01.png)


## 2. 「如果我打汉化补丁，我能被封号吗？」

### 有些问题属于逻辑上的事情，这可以算一种

@359376207

![ScreenShot](/Preview/Q02.png)

@佐手丶死亡

![ScreenShot](/Preview/Q03.png)

## 3. 「正版能用？」

### 你怀疑的根据在哪？你有考证过吗？

@我是你偶像ii  

![ScreenShot](/Preview/Q04.png)

## 4. 中文好难, isn't it？

@丶棠哥Cc

![ScreenShot](/Preview/Q05.png)

@零镜灬

![ScreenShot](/Preview/Q06.png)

@徐文轩啊

![ScreenShot](/Preview/Q07.png)

## 5. 「汉化用不了」、「不会用汉化」、「汉化失败」

### 现况就是没问题，我装了，开了，没崩，可以正常显示，可以更新，所以你回报说不能用，我只能说很奇怪。

@初代vs杰顿

![ScreenShot](/Preview/Q08.png)

@黎科光电

![ScreenShot](/Preview/Q09.png)

@逻辑外的逻辑里

![ScreenShot](/Preview/Q10.png)

@对世界说谎i

![ScreenShot](/Preview/Q11.png)

@叫我主帅大人

![ScreenShot](/Preview/Q12.png)

## 6.状况都没搞清楚就说是汉化的锅

@acarand

![ScreenShot](/Preview/Q13.png)