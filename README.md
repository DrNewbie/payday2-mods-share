# 你非常需要知道的事情

1. 我没兴趣帮解决问题。

2. MOD多为网路上爬来的。

3. Have Fun!

# 下载

*   [MOD打包下载 (2023-06-14)](/Download/Release.zip)

*   [SuperBLT](/Download/payday2bltwsockdll-3.3.8.zip)

*   [Legendary Armour Skins](/Download/Legendary%20Armour%20Skins.7z) by [Cpone](https://modworkshop.net/mod/24006)

*   [Microsoft Visual C++ 2017 Redistributable package (x86)](/Download/vc_redist.x86.zip)

*   [《收获日2：传承合集》PDLG汉化](/Download/%E3%80%902019.12.18%E6%9B%B4%E6%96%B0%E3%80%91%E3%80%8A%E6%94%B6%E8%8E%B7%E6%97%A52%E3%80%8BPDLG%E6%B1%89%E5%8C%96.7z)

*   [《收获日2：传承合集》PDLG汉化](https://tieba.baidu.com/p/6331905538)

*   [DAHM/DorHUD - PD:TH](/Download/dahm-public-release-202110182044.zip)

# List

*   [SuperBLT](https://superblt.znix.xyz/)
*   [Advanced Movement Standalone by Solo Queue Pixy](https://modworkshop.net/mod/24736)
*   [Auto Daily-Skin Dropper](https://modworkshop.net/mod/22559)
*   [Anti Voice Spam by Offyerrocker](https://modworkshop.net/mod/23361)
*   [Bandwidth Saver by Hoppip](https://modworkshop.net/mod/41495)
*   [Bipods That (Actually) Work by Offyerrocker](https://modworkshop.net/mod/21285)
*   [BeardLib](https://modworkshop.net/mod/14924)
*   [Better Bots by Schmuddel](https://modworkshop.net/mod/12736)
*   [Bot Weapons by Hoppip](https://modworkshop.net/mod/12852)
*   [Better Boost Icons by HONOUR AMONGST THIEVES](https://modworkshop.net/mod/24808)
*   [Clear Texture Cache by TdlQ](https://pdmods-arc.berigora.net/paydaymods.com/mods/662/CTC.html)
*   [Create Empty Lobby by Snh20](https://modworkshop.net/mod/14791)
*   [Disable Weapon Preview Rotation by Offyerrocker](https://modworkshop.net/mod/21829)
*   [Disable Fog by rrti](https://modworkshop.net/mod/17459)
*   [Down Counter by Offyerrocker](https://modworkshop.net/mod/21994)
*   [Drag and Drop Inventory by TdlQ](https://modworkshop.net/mod/14217)
*   [Enemy Spawner by Zdann](https://modworkshop.net/mod/20663)
*   [Enemy Health and Info by Undeadsewer](https://modworkshop.net/mod/15157)
*   [Extra Heist Info by Dom](https://modworkshop.net/mod/31915)
*   Faster Skip
*   [Full Speed Swarm by TdlQ](https://modworkshop.net/mod/17213)
*   [GameInfoManager by pjal3urb](https://bitbucket.org/pjal3urb/gameinfomanager)
*   Goonmod's Custom Waypoints
*   [HopLib by Hoppip](https://modworkshop.net/mod/21431)
*   [Hostage Pathing Fix by Schmuddel](https://modworkshop.net/mod/16753)
*   [Hide "Clear Progress" Button by Offyerrocker](https://modworkshop.net/mod/25937)
*   [Hide Buy DLC Update 198 by test1](https://modworkshop.net/mod/25942)
*   [(OFF) HUDList by pjal3urb](https://bitbucket.org/pjal3urb/hudlist)
*   [Iter by TdlQ](https://pdmods-arc.berigora.net/paydaymods.com/mods/433/ITR.html)
*   [Keepers by TdlQ](https://pdmods-arc.berigora.net/paydaymods.com/mods/102/KPR.html)
*   [Ladder Improvements by Offyerrocker](https://modworkshop.net/mod/23192)
*   [Lasers+ by Offyerrocker](https://modworkshop.net/mod/20577)
*   [Mod List Lite by BangL](https://modworkshop.net/mod/20977)
*   [(OFF) MUI by Armithaig](https://steamcommunity.com/app/218620/discussions/15/620713633850948964/)
*   [No Contract Broker Images by Xeletron™](https://modworkshop.net/mod/21115)
*   No Duplicated Bullets
*   No Post Processing
*   [No Flashlight Glow by Offyerrocker](https://modworkshop.net/mod/22410)
*   [No Red Player Lasers by Offyerrocker](https://modworkshop.net/mod/21990)
*   [No Prints by Sora](https://modworkshop.net/mod/21549)
*   More Reputation
*   [Two-Click Safe House by Snh20](https://pdmods-arc.berigora.net/paydaymods.com/mods/417/twoclicksafehouse.html)
*   [Press-2-Hold by Iron Predator](https://modworkshop.net/mod/13489)
*   [Primary As Secondary by Cpone](https://modworkshop.net/mod/24361)
*   [Scrollable menus by Luffy](https://pdmods-arc.berigora.net/paydaymods.com/mods/462/ScrollableMenusUpdates.html)
*   [Secondary As Primary by Cpone](https://modworkshop.net/mod/24360)
*   [Sell All Items by Offyerrocker](https://modworkshop.net/mod/24537)
*   Automatic Skip Screen/auto card pick by Seven
*   [Straight To Main Menu by Snh20](https://pdmods-arc.berigora.net/paydaymods.com/mods/215/straighttomainmenu.html)
*   [The Fixes by andole](https://modworkshop.net/mod/23732)
*   [Tweakdata Fixed by Hassat Hunter](https://modworkshop.net/mod/21098)
*   [TdlQ's mods](https://pd2mods.z77.fr)
*   [Useful Bots by Hoppip](https://modworkshop.net/mod/31221)
*   [VanillaHUD Plus by test1](https://modworkshop.net/mod/25629)

# Preview

![ScreenShot](/Preview/P01.jpg)

![ScreenShot](/Preview/P02.jpg)
